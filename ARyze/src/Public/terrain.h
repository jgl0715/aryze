#pragma once

#include "core/core.h"
#include "rendering/rendering.h"
#include "sandbox.h"

#define TERRAIN_WIDTH 40.0f
#define TERRAIN_LENGTH 30.0f

class KinectSandboxWorker;

class Terrain
{
public:

	Terrain(KinectCalibration calib);
	~Terrain();

	Ry::Shader* get_shader() const;

	void update();
	void render();

private:

	KinectCalibration calib;

	KinectSandboxWorker* worker;

	Ry::Mesh* mesh;
	Ry::Texture* hm_texture;
	Ry::Shader* shader;
};