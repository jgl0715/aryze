#pragma once

#include "rendering/rendering.h"
#include "math/math.h"
#include "rendering/camera.h"
#include "application.h"
#include "input.h"
#include "timer.h"
#include "sandbox.h"

class Terrain;
class SandboxWorker;

class SandboxVisual : public Ry::AbstractGame, Ry::ScrollListener, Ry::ScreenSizeListener
{
public:

	Terrain* terrain;

	Ry::Shader* shader = nullptr;
	Ry::Mesh* sandbox = nullptr;

	Ry::Transform* sandbox_transform;
	Ry::Transform* terrain_transform;

	Ry::PerspectiveCamera* camera;

	Ry::Timer dead_zone;

	Ry::Vector3 focus_dir;
	Ry::Vector3 focus_point;
	bool focused;
	float focus_distance;

	SandboxVisual();
	~SandboxVisual();

	// Window callbacks
	virtual void resized(int new_width, int new_height);

	// Input callbacks
	void onScroll(double scroll);
	
	// Game callbacks
	void init() override;
	void update(float delta) override;
	void render() override;
	void quit() override;

private:

	ProjectorCalibration projector_calib;
	KinectCalibration kinect_calib;
	PlaneCalibration plane_calib;

};