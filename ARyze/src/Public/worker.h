#pragma once

#include "core/core.h"
#include "core/string.h"
#include <thread>
#include <mutex>
#include "sandbox.h"

class Kinect;

class SandboxWorker
{
public:

	SandboxWorker(int32 heightmap_width, int32 heightmap_length);
	~SandboxWorker();

	virtual void work() = 0;
	virtual float* get_data();
	virtual void stop();

	void begin();
	void end();

	int32 get_hm_width();
	int32 get_hm_height();

	std::mutex& get_mutex();

protected:

	bool run;
	std::lock_guard<std::mutex>* lock;
	std::mutex front_buffer_mutex;
	std::mutex back_buffer_mutex;
	std::thread worker_thread;
	float* front_buffer;
	float* back_buffer;
	uint32 heightmap_width;
	uint32 heightmap_length;

	void swap_buffers();
};

class KinectSandboxWorker : public SandboxWorker
{
public:

	KinectSandboxWorker(int32 start_x, int32 start_y, int32 heightmap_width, int32 heightmap_length);
	~KinectSandboxWorker();
	virtual void work();

	void set_calib(KinectCalibration calib);
	void set_bounds(int32 sx, int32 sy, int32 w, int32 h);

	uint16 get_min();
	uint16 get_max();

	void set_min(uint16 min);
	void set_max(uint16 max);

	int32 ticks() { return this->num_ticks; }

	Ry::Vector3 get_camera_space(int32 d_x, int32 d_y) const;

private:

	volatile int32 num_ticks;

	uint16 minimum;
	uint16 maximum;
	
	int32 sx;
	int32 sy;
	Kinect* kinect;
};

class PerlinSandboxWorker : public SandboxWorker
{
public:

	PerlinSandboxWorker(int32 heightmap_width, int32 heightmap_length);
	~PerlinSandboxWorker();
	virtual void work();

};

KinectSandboxWorker* make_worker_from_calibration(const KinectCalibration& calib_path);