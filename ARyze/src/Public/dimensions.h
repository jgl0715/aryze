#pragma once

#define IN_TO_CM(VAL) (VAL * 2.54f)
#define CM_TO_IN(VAL) (VAL / 2.54f)
#define CM_TO_MM(VAL) (VAL * 10.0f)
#define IN_TO_MM(VAL) CM_TO_MM(IN_TO_CM(VAL))

#define KINECT_START 500
#define BOX_BOTTOM IN_TO_MM(56.5)
#define BOX_HEIGHT IN_TO_MM(9)
#define BOX_TOP (BOX_BOTTOM - BOX_HEIGHT)
#define MAX_SAND_HEIGHT (BOX_BOTTOM - IN_TO_MM(15))

#define DEPTH_WIDTH 512
#define DEPTH_HEIGHT 424