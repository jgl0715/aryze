#pragma once

#include "math/math.h"

struct PlaneCalibration
{
	Ry::Vector3 plane_normal;
	float plane_offset;
};

struct KinectCalibration
{
	int32 x;
	int32 y;
	int32 w;
	int32 h;
};

struct ProjectorCalibration
{
	Ry::Matrix4 view_projection;
};

void load_calibration(PlaneCalibration& plane_calib, KinectCalibration& kinect_calib, ProjectorCalibration& projector_calib);