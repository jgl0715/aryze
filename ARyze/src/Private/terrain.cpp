#include "terrain.h"
#include "globals.h"
#include "profiler.h"
#include "worker.h"
#include <iostream>

Terrain::Terrain(KinectCalibration calib):
	calib(calib)
{
	hm_texture = Ry::rapi->make_texture();

	shader = Ry::rapi->make_shader(Ry::VF1P1UV1C, "visual.glv", "visual.glf");

	worker = make_worker_from_calibration(calib);

	// Generate a mesh that is the same resolution as the height map
	int32 hw = calib.w / 2;
	int32 hh = calib.h / 2;
	mesh = new Ry::Mesh(Ry::VF1P1UV1C);
	
	float sx = -TERRAIN_WIDTH / 2;
	float sz = -TERRAIN_LENGTH / 2;
	float width = TERRAIN_WIDTH / calib.w;
	float length = TERRAIN_LENGTH / calib.h;

	// busy loop to wait for thread to initialize
	while (worker->ticks() < 20)
	{
		std::cout << worker->ticks() << std::endl;
	}
	
	for (int32 j = 0; j < calib.h; j++)
	{
		for (int32 i = 0; i < calib.w; i++)
		{
			Ry::Vector3 camera_space = worker->get_camera_space(i, j);
		//	camera_space.x *= scale_x;
		//	camera_space.y *= scale_y;
			
			//camera_space.x = sx + i * width;
		//	camera_space.y = sz + j * length;
			float u = (float)i / (calib.w - 1);
			float v = (float)j / (calib.h - 1);

			mesh->add_vertex(camera_space.x, 0.0f, camera_space.y, u, v, 1.0f, 1.0f, 1.0f, 1.0f);

			if (i < calib.w - 1 && j < calib.h - 1)
			{
				int32 top_left = (j + 0) * calib.w + (i + 0);
				int32 top_right = (j + 0) * calib.w + (i + 1);
				int32 bottom_left = (j + 1) * calib.w + (i + 0);
				int32 bottom_right = (j + 1) * calib.w + (i + 1);

				mesh->add_tri(top_left, top_right, bottom_right);
				mesh->add_tri(bottom_right, bottom_left, top_left);
			}
		}
	}
	mesh->update();
	mesh->set_shader(0, shader);
}

Terrain::~Terrain()
{
	mesh->deleteMesh();
	delete mesh;
}

Ry::Shader* Terrain::get_shader() const
{
	return shader;
}

void Terrain::update()
{
	worker->begin();
	hm_texture->data(worker->get_data(), Ry::PixelFormat::RED, calib.w, calib.h);
	worker->end();
}

void Terrain::render()
{

	hm_texture->bind();
	mesh->render(Ry::Primitive::TRIANGLE);
}