#include "core/core.h"
#include "worker.h"
#include <kinect_base.h>
#include "dimensions.h"
#include "FastNoise/fast_noise.h"
#include <iostream>

SandboxWorker::SandboxWorker(int32 heightmap_width, int32 heightmap_length):
	worker_thread(&SandboxWorker::work, this),
	heightmap_width(heightmap_width),
	heightmap_length(heightmap_length),
	run(true)
{
	front_buffer = new float[(int64) heightmap_width * heightmap_length];
	back_buffer = new float[(int64)heightmap_width * heightmap_length];
}

SandboxWorker::~SandboxWorker()
{

}

void SandboxWorker::stop()
{
	run = false;

	worker_thread.join();
}

std::mutex& SandboxWorker::get_mutex()
{
	return front_buffer_mutex;
}

float* SandboxWorker::get_data()
{
	return front_buffer;
}

void SandboxWorker::swap_buffers()
{
	// Copy the back buffer to the front buffer in a thread safe manner.
	// Lock automatically unlocks at the end of this scope
	std::lock_guard<std::mutex> buffer_lock(front_buffer_mutex);
	memcpy_s(front_buffer, (int64) heightmap_width * heightmap_length* sizeof(float), back_buffer, (int64) heightmap_width * heightmap_length * sizeof(float));
}

void SandboxWorker::begin()
{
	lock = new std::lock_guard<std::mutex>(front_buffer_mutex);
}

void SandboxWorker::end()
{
	delete lock;

	lock = nullptr;
}

int32 SandboxWorker::get_hm_width()
{
	return heightmap_width;
}

int32 SandboxWorker::get_hm_height()
{
	return heightmap_length;
}

KinectSandboxWorker::KinectSandboxWorker(int32 start_x, int32 start_y, int32 heightmap_width, int32 heightmap_length) :
	SandboxWorker(heightmap_width, heightmap_length),
	sx(start_x),
	sy(start_y),
	kinect(nullptr)
{
	minimum = MAX_SAND_HEIGHT;
	maximum = BOX_BOTTOM;
	this->num_ticks = 0;
}

KinectSandboxWorker::~KinectSandboxWorker()
{
	
}

uint16 KinectSandboxWorker::get_min()
{
	return minimum;
}

uint16 KinectSandboxWorker::get_max()
{
	return maximum;
}

void KinectSandboxWorker::set_min(uint16 min)
{
	minimum = min;
}

void KinectSandboxWorker::set_max(uint16 max)
{
	maximum = max;
}

Ry::Vector3 KinectSandboxWorker::get_camera_space(int32 d_x, int32 d_y) const
{
	float cam_x = 0;
	float cam_y = 0;
	float cam_z = 0;

	if(d_x >= 0 && d_x < DEPTH_WIDTH && d_y >= 0 && d_y < DEPTH_HEIGHT)
	{
		kinect->depth_to_camera(d_x, DEPTH_HEIGHT - d_y - 1, cam_x, cam_y, cam_z);

		return Ry::Vector3(cam_x, cam_y, cam_z);
	}

	return Ry::Vector3();
}

void KinectSandboxWorker::work()
{
	kinect = new Kinect(DEPTH_WIDTH, DEPTH_HEIGHT);
	kinect->init();
	
	while (run)
	{
		// Updates the kinect. This pulls the next depth frame.
		kinect->update();
		num_ticks++;

		std::lock_guard<std::mutex> buffer_lock_back(back_buffer_mutex);
		for (int32 j = 0; j < heightmap_length; j++)
		{
			for (int32 i = 0; i < heightmap_width; i++)
			{
				uint32 index = j * heightmap_width + i;

				// Read the depth data from the kinect.
				// TODO: some heavy signal processing needs to be done here
				uint16 depth_data = kinect->get_depth_data_raw(sx + i, DEPTH_HEIGHT - (sy + j) - 1);

				// Defaults to max depth
				back_buffer[index] = 1.0f;
				
				// Zero data usually indicates that an object is too close, keep the same value from last frame
				if (depth_data > 0)
				{

					//std::cout << depth_data << std::endl;

					// Gets the distance from the theoretical max height of the sand
					// TODO: this max sand height should come from the calibration!!!
					float dist_from_top = (float)(depth_data - minimum);

					// Clamp the distance. If this is less than zero, then the sand is higher than the max value.
					if (dist_from_top < 0.0f)
						dist_from_top = 0.0f;

					// This is the range the the sand can be built within (the bottom of the box to the max sand height)
					float range = maximum - minimum;

					// Normalize the distance, and then invert it as the top should be 1.0 and the bottom should be 0.0
					float val = 1.0f - dist_from_top / range;
					
					// Clamps the value
					if (val >= 0.99999f)
						val = 0.99999f;

					if (isinf(val))
					{
						std::cout << "a val is inf" << std::endl;
					}

					back_buffer[index] = val;
				}
			}
		}

		swap_buffers();
	}
}

void KinectSandboxWorker::set_calib(KinectCalibration calib)
{
	std::lock_guard<std::mutex> buffer_lock_back(back_buffer_mutex);
	std::lock_guard<std::mutex> buffer_lock_front(front_buffer_mutex);

	this->sx = calib.x;
	this->sy = calib.y;
	this->heightmap_width = calib.w;
	this->heightmap_length = calib.h;

	if (front_buffer)
		delete front_buffer;

	if (back_buffer)
		delete back_buffer;

	front_buffer = new float[(int64)heightmap_width * heightmap_length];
	back_buffer = new float[(int64)heightmap_width * heightmap_length];
}

void KinectSandboxWorker::set_bounds(int32 sx, int32 sy, int32 w, int32 h)
{
	std::lock_guard<std::mutex> buffer_lock_back(back_buffer_mutex);
	std::lock_guard<std::mutex> buffer_lock_front(front_buffer_mutex);

	this->sx = sx;
	this->sy = sy;
	this->heightmap_width = w;
	this->heightmap_length = h;

	if (front_buffer)
		delete front_buffer;

	if (back_buffer)
		delete back_buffer;

	front_buffer = new float[(int64)heightmap_width * heightmap_length];
	back_buffer = new float[(int64)heightmap_width * heightmap_length];
}

PerlinSandboxWorker::PerlinSandboxWorker(int32 heightmap_width, int32 heightmap_length):
	SandboxWorker(heightmap_width, heightmap_length)
{

}

PerlinSandboxWorker::~PerlinSandboxWorker()
{

}

void PerlinSandboxWorker::work()
{
	FastNoise noise;
	noise.SetNoiseType(FastNoise::Perlin);

	while (run)
	{

		for (int32 j = 0; j < heightmap_length; j++)
		{
			for (int32 i = 0; i < heightmap_width; i++)
			{
				uint32 index = j * heightmap_width + i;

				float depth_data = (noise.GetNoise(i * 3.6f, j * 3.6f) + 1.0f) / 2.0f;
				back_buffer[index] = depth_data;
			}
		}

		swap_buffers();
	}
}

KinectSandboxWorker* make_worker_from_calibration(const KinectCalibration& kinect_calib)
{
	KinectSandboxWorker* result = new KinectSandboxWorker(kinect_calib.x, kinect_calib.y, kinect_calib.w, kinect_calib.h);

	return result;
}