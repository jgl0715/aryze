#include "visual.h"
#include "core/core.h"
#include "desktop_app.h"
#include "rendering/rendering.h"
#include "rendering/camera.h"
#include "globals.h"
#include <iostream>
#include "math/vector.h"
#include "terrain.h"
#include <cstdlib>
#include "sandbox.h"

SandboxVisual::SandboxVisual()
{
	this->camera = nullptr;
	this->focused = false;
	this->focus_dir = Ry::Vector3(0.0f, 0.0f, -1.0f);
	this->focus_distance = 5.0f;
	this->dead_zone.set_delay(0.1);
}

SandboxVisual::~SandboxVisual()
{

}

void SandboxVisual::resized(int32 width, int32 height)
{
	Ry::rapi->set_viewport(0, 0, width, height);
	camera->resize(width, height);
}

void SandboxVisual::onScroll(double scroll)
{
	if (focused)
	{
		focus_distance -= scroll;
	}
	else
	{
		camera->transform.position += camera->transform.get_forward() * scroll * 2.0f;
	}
}

void SandboxVisual::init()
{
	load_calibration(plane_calib, kinect_calib, projector_calib);

	Ry::app->add_screen_size_listener(this);

	// Setup shaders
	shader = Ry::rapi->make_shader(Ry::VF1P1UV1C, "phong_unlit.glv", "phong_unlit.glf");

	// Setup meshes
	this->terrain = new Terrain(kinect_calib);
	this->sandbox = Ry::load_obj("mesh/tinker.obj", "mesh/obj.mtl");
	this->sandbox->set_shader_all(shader);

	// Setup transforms
	this->sandbox_transform = new Ry::Transform;
	this->terrain_transform = new Ry::Transform;
	this->sandbox_transform->rotation.x = -90.0f;
	this->sandbox_transform->scale = Ry::Vector3(0.1574f, 0.1574f, 0.1574f);
	this->terrain_transform->position.y = 4.0f;
	this->terrain_transform->scale.x = 10.0f;
	this->terrain_transform->scale.z = 10.0f;


	// Setup camera
	camera = new Ry::PerspectiveCamera(4.0f / 3.0f, 70.0f, 0.1f, -100.0f);
	camera->transform.position.z = -5.0f;

	// Setup input
	Ry::input_handler->addScrollListener(this);

}

void SandboxVisual::update(float delta)
{
	terrain->update();

	float dx = Ry::input_handler->getDx();
	float dy = Ry::input_handler->getDy();

	// Gives each movement command a fair change, including ones that involve multiple buttons.
	if (!Ry::input_handler->isButtonDown(0) && !Ry::input_handler->isButtonDown(1))
	{
		dead_zone.restart();
	}

	if (Ry::input_handler->isButtonDown(0) && Ry::input_handler->isButtonDown(1) && (focused || dead_zone.is_ready()))
	{
		focus_dir = normalized(rot_vec(focus_dir, Ry::Vector3(0.0f, 1.0f, 0.0f), -dx * 0.2f));

		Ry::Vector3 rotation_pitch = normalized(rot_vec(focus_dir, camera->transform.get_right(), dy * 0.2f));

		if (angle(rotation_pitch, Ry::Vector3(0.0f, 1.0f, 0.0f)) > 10.0f && angle(rotation_pitch, Ry::Vector3(0.0f, -1.0f, 0.0f)) > 10.0f)
		{
			focus_dir = rotation_pitch;
		}

		focused = true;
		dead_zone.restart();
	}

	// Move in the XY plane (pan)
	else if (Ry::input_handler->isButtonDown(0) && (!focused || dead_zone.is_ready()))
	{
		Ry::Vector3 up = camera->transform.get_up();
		Ry::Vector3 right = camera->transform.get_right();

		camera->transform.position += up * (dy / 30.0f);
		camera->transform.position += right * (dx / 30.0f);

		focused = false;
	}

	// Rotate
	else if (Ry::input_handler->isButtonDown(1) && (!focused || dead_zone.is_ready()))
	{
		camera->transform.rotation.y += dx / 5.0f;
		camera->transform.rotation.x += dy / 5.0f;

		focused = false;
		dead_zone.restart();
	}

	// Move in the XZ plane
	else if (Ry::input_handler->isButtonDown(3) && dead_zone.is_ready())
	{
		Ry::Vector3 forward = Ry::Vector3(0.0f, 1.0f, 0.0f);
		Ry::Vector3 right = camera->transform.get_right();

		camera->transform.position += forward * (dy / 30.0f);
		camera->transform.position -= right * (dx / 30.0f);
		focused = false;
	}

	if (!focused)
	{
		// Calculate focus information for when we focus
		focus_dir = camera->transform.position - focus_point;
		focus_distance = magnitude(focus_dir);
		normalize(focus_dir);
	}
	else
	{
		camera->transform.position = focus_dir * focus_distance;
		camera->transform.rotation = make_rot(focus_dir * 1.0f);
	}
}

void SandboxVisual::render()
{
	// Setup rendering
	Ry::rapi->set_render_mode(Ry::RenderMode::FILLED);
	
	terrain->get_shader()->uniformMat44("view_proj", camera->get_view_proj());
	terrain->get_shader()->uniformMat44("model", terrain_transform->get_transform());
	terrain->render();

	// Setup rendering
	Ry::rapi->set_render_mode(Ry::RenderMode::FILLED);
	shader->uniformMat44("view_proj", camera->get_view_proj());
	shader->uniformMat44("model", sandbox_transform->get_transform());
	sandbox->render(Ry::Primitive::TRIANGLE);
}

void SandboxVisual::quit()
{

}