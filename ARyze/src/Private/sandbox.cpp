#include "sandbox.h"
#include "util.h"
#include <iostream>
#include "file/file.h"
#include "math/math.h"

void load_calibration(PlaneCalibration& plane_calib, KinectCalibration& kinect_calib, ProjectorCalibration& projector_calib)
{
	if (Ry::File::does_file_exist("./calibration.dat"))
	{
		Ry::String* lines;
		Ry::String* projector_tokens0;
		Ry::String* projector_tokens1;
		Ry::String* projector_tokens2;
		Ry::String* projector_tokens3;
		Ry::String* kinect_tokens;
		Ry::String calibration = Ry::File::load_file_as_string("./calibration.dat");

		if (calibration.split("\n", &lines) < 5)
		{
			std::cerr << "Calibration file consists of four lines for the projector calibration followed by one line for the kinect calibration" << std::endl;
			return;
		}

		lines[0].split(" ", &kinect_tokens);
		lines[1].split(" ", &projector_tokens0);
		lines[2].split(" ", &projector_tokens1);
		lines[3].split(" ", &projector_tokens2);
		lines[4].split(" ", &projector_tokens3);

		// Load the projection matrix
		projector_calib.view_projection[0][0] = projector_tokens0[0].to_float();
		projector_calib.view_projection[0][1] = projector_tokens0[1].to_float();
		projector_calib.view_projection[0][2] = projector_tokens0[2].to_float();
		projector_calib.view_projection[0][3] = projector_tokens0[3].to_float();
		projector_calib.view_projection[1][0] = projector_tokens1[0].to_float();
		projector_calib.view_projection[1][1] = projector_tokens1[1].to_float();
		projector_calib.view_projection[1][2] = projector_tokens1[2].to_float();
		projector_calib.view_projection[1][3] = projector_tokens1[3].to_float();
		projector_calib.view_projection[2][0] = projector_tokens2[0].to_float();
		projector_calib.view_projection[2][1] = projector_tokens2[1].to_float();
		projector_calib.view_projection[2][2] = projector_tokens2[2].to_float();
		projector_calib.view_projection[2][3] = projector_tokens2[3].to_float();
		projector_calib.view_projection[3][0] = projector_tokens3[0].to_float();
		projector_calib.view_projection[3][1] = projector_tokens3[1].to_float();
		projector_calib.view_projection[3][2] = projector_tokens3[2].to_float();
		projector_calib.view_projection[3][3] = projector_tokens3[3].to_float();

//		projector_calib.x = projector_tokens[0].to_int32();
//		projector_calib.y = projector_tokens[1].to_int32();
//		projector_calib.w = projector_tokens[2].to_int32();
//		projector_calib.h = projector_tokens[3].to_int32();

		kinect_calib.x = kinect_tokens[0].to_int32();
		kinect_calib.y = kinect_tokens[1].to_int32();
		kinect_calib.w = kinect_tokens[2].to_int32();
		kinect_calib.h = kinect_tokens[3].to_int32();
	}
	else
	{
		projector_calib.view_projection = Ry::id4();

		kinect_calib.x = 0;
		kinect_calib.y = 0;
		kinect_calib.w = 512;
		kinect_calib.h = 424;
	}

}