#pragma once

#include "application.h"
#include "input.h"
#include "math/math.h"
#include "sandbox.h"
#include <opencv2/core/types.hpp>
#include "worker.h"

#define WIDTH 512
#define HEIGHT 424

#define AVG_COUNT 50

// Create a 4x3 grid of tie points
#define HORIZONTAL_POINTS 2
#define VERTICAL_POINTS 2

namespace Ry
{
	class TextureBatch;
	class ShapeBatch;
	class Texture;
	class OrthoCamera;
}

struct TiePoint
{
	Ry::Vector3 CameraSpace;
	Ry::Vector2 ProjectorSpace;
};

struct AverageFrameData
{
	bool recording_frame;
	bool has_recorded;
	int32 frame_to_record;
	float* avg_frame;
};

enum CalibrationPhase
{
	KINECT,
	PROJECTOR
};

class SandboxWorker;

class CalibrateBox : public Ry::AbstractGame, Ry::ButtonListener, Ry::ScreenSizeListener, Ry::KeyListener
{

public:

	Ry::TextureBatch* texture_batch;
	Ry::ShapeBatch* shape_batch;
	Ry::OrthoCamera* camera;

	CalibrateBox();

	Ry::Vector2 get_projector_space_point();
	Ry::Vector2 get_projector_space_point_norm();

	void begin_record();

	// Window listeners
	void resized(int new_width, int new_height);

	// Input listeners
	void onButtonPressed(int key);
	void onButtonReleased(int key);
	void onKeyPressed(int32 key);
	void onKeyReleased(int key);

	virtual void init() override;
	virtual void update(float delta) override;
	virtual void render() override;
	virtual void quit() override;

private:

	bool recorded;
	float elap;

	KinectCalibration Kinect;
	ProjectorCalibration Proj;

	void load_current();
	void finish_calibration();

	Ry::Matrix4 homography;

	CalibrationPhase phase;

	float cx;
	float cy;

	float get_drag_box_x();
	float get_drag_box_y();
	float get_drag_box_w();
	float get_drag_box_h();

	// Tie points
	std::vector<TiePoint> tie_points;

	// Keypoints found by OpenCV for blob detection
	// These are the current tie point we are on
	int32 horizontal_point;
	int32 vertical_point;
	bool calibration_complete;
	bool testing;

	// Kinect feed texture
	Ry::Texture* texture;
	KinectSandboxWorker* worker;

	bool selected;
	bool dragging;

	// For defining blobs
	Ry::Vector2 rect_start;
	Ry::Vector2 rect_end;

	// Average frame for plane equation
	AverageFrameData frame_data;
	float* foreground_frame;

	// These functions are used for the recording of an average background frame
	void record_frames(float* data);
	void process_avg();

	void handle_drag();

	void save_calib();

};