#include "calibrate.h"
#include <iostream>
#include <rendering/2d/batch.h>
#include <rendering/interface/shader.h>
#include <rendering/interface/texture.h>
#include <rendering/mesh.h>
#include "rendering/camera.h"
#include "rendering/rendering.h"
#include "file/file.h"
#include <util.h>
#include <input.h>
#include <globals.h>
#include <math/math.h>
#include "worker.h"
#include "util.h"
#include "desktop_app.h"
#include "keys.h"
#include "opencv2/core.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/calib3d/calib3d.hpp"

#include "QRSolve/qr_solve.h"

CalibrateBox::CalibrateBox()
{
	frame_data.recording_frame = false;
	frame_data.has_recorded = false;
	frame_data.frame_to_record = 0;
	frame_data.avg_frame = nullptr;
	horizontal_point = 0;
	vertical_point = 0;
	calibration_complete = false;
	phase = CalibrationPhase::KINECT;

	foreground_frame = new float[WIDTH * HEIGHT];

	elap = 0.0f;
	recorded = false;
}

void CalibrateBox::onButtonPressed(int button)
{
	if(phase == CalibrationPhase::KINECT && button == 0)
	{
		dragging = true;

		float sx = Ry::input_handler->getX();
		float sy = Ry::app->get_height() - Ry::input_handler->getY() - 1;

		rect_start.x = sx;
		rect_start.y = sy;
	}
}

void CalibrateBox::onButtonReleased(int button)
{
	if(phase == CalibrationPhase::KINECT && button == 0 && dragging)
	{
		dragging = false;
		selected = false;

		// Update sandbox worker to only contain selected area
		float scale_x = (float)worker->get_hm_width() / Ry::app->get_width();
		float scale_y = (float)worker->get_hm_height() / Ry::app->get_height();
		Kinect.x = get_drag_box_x() * scale_x;
		Kinect.y = worker->get_hm_height() - (get_drag_box_y() + get_drag_box_h()) * scale_y - 1;
		Kinect.w = (int32) (get_drag_box_w() * scale_x);
		Kinect.h = (int32) (get_drag_box_h() * scale_y);
		worker->set_bounds(Kinect.x, Kinect.y, Kinect.w, Kinect.h);

		delete foreground_frame;
		foreground_frame = new float[Kinect.w * Kinect.h];

		// Render to the projector now for grid calibration
		Ry::DesktopApp* desktop_app = dynamic_cast<Ry::DesktopApp*>(Ry::app);
		if(desktop_app)
		{
			desktop_app->set_fullscreen(true, 1);
			phase = CalibrationPhase::PROJECTOR;
		}
	}
}

void CalibrateBox::onKeyPressed(int32 key)
{
	if(key == KEY_1)
	{
		load_current();
	}
	
	if (key == KEY_ENTER && phase == CalibrationPhase::PROJECTOR)
	{
		// Create a tie point and add it to the list
		TiePoint point;
		int32 depth_x = (int32)cx + Kinect.x;
		int32 depth_y = (int32)cy + Kinect.y;
		point.CameraSpace = worker->get_camera_space(depth_x, depth_y);
		point.ProjectorSpace = get_projector_space_point_norm();
		tie_points.push_back(point);

		std::cout << "TIE POINT CAPTURED" << std::endl;
		std::cout << "Camera (from depth): " << point.CameraSpace.x << " " << point.CameraSpace.y << " " << point.CameraSpace.z << std::endl;
		std::cout << "Projector: " << point.ProjectorSpace.x << " " << point.ProjectorSpace.y << std::endl;

		// Move to next calibration point
		horizontal_point++;

		if (horizontal_point >= HORIZONTAL_POINTS)
		{
			vertical_point++;
			horizontal_point = 0;
		}

		if(vertical_point >= VERTICAL_POINTS)
		{
			calibration_complete = true;
			vertical_point = 0;
		}
	}

	if(key == KEY_R)
	{
		begin_record();
	}
}

void CalibrateBox::onKeyReleased(int key)
{

}

void CalibrateBox::begin_record()
{
	// Initialize recording average frame
	frame_data.recording_frame = true;
	frame_data.frame_to_record = 0;

	// J.Leavell: TODO - This was for allowing the user to select a subsection. I am not totally sure if this functionality is necessary.
	/*
		float scale_x = ((float)WIDTH) / Ry::app->get_width();
		float scale_y = ((float)HEIGHT) / Ry::app->get_height();
		avg_width = (int32)(((rect_end.x - rect_start.x)) * scale_x);
		avg_height = (int32)(((rect_end.y - rect_start.y)) * scale_y);
	*/

	// Allocate memory for average frame and initialize the entire memory to zero
	frame_data.avg_frame = new float[WIDTH * HEIGHT];
	memset(frame_data.avg_frame, 0, WIDTH * HEIGHT * sizeof(float));
}

Ry::Vector2 CalibrateBox::get_projector_space_point()
{
	float edge = 0.3f;
	float CH_X = (float)(Ry::app->get_width() * (1-2*edge)) / (HORIZONTAL_POINTS-1) * horizontal_point + Ry::app->get_width() * edge;
	float CH_Y = (float)(Ry::app->get_height() * (1-2*edge)) / (VERTICAL_POINTS-1) * vertical_point + Ry::app->get_height() * edge;

	return Ry::Vector2(CH_X, CH_Y);
}

Ry::Vector2 CalibrateBox::get_projector_space_point_norm()
{
	Ry::Vector2 vec = get_projector_space_point();

	vec.x = (vec.x - Ry::app->get_width() / 2.0f) / (Ry::app->get_width() / 2.0f);
	vec.y = -(vec.y - Ry::app->get_height() / 2.0f) / (Ry::app->get_height() / 2.0f);

	std::cout << "size " << Ry::app->get_width() << " " << Ry::app->get_height() << std::endl;

	return vec;
}

void CalibrateBox::resized(int new_width, int new_height)
{
	camera->resize(new_width, new_height);

	Ry::rapi->set_viewport(0, 0, new_width, new_height);
}

void CalibrateBox::init()
{


	// Register input handlers
	Ry::input_handler->addButtonListener(this);
	Ry::input_handler->addKeyListener(this);

	Ry::app->add_screen_size_listener(this);

	worker = new KinectSandboxWorker(0, 0, WIDTH, HEIGHT);
	worker->set_min(50);
	worker->set_max(2000);

	camera = new Ry::OrthoCamera(0, Ry::app->get_width(), 0, Ry::app->get_height());

	shape_batch = new Ry::ShapeBatch;
	texture_batch = new Ry::TextureBatch;
	texture = Ry::rapi->make_texture();
}

void CalibrateBox::update(float delta)
{
	elap += delta;

	if(elap >= 5.0f && !recorded)
	{		
		recorded = true;
	}
	
	if(phase == CalibrationPhase::KINECT)
	{
		handle_drag();
	}
	else if(phase == CalibrationPhase::PROJECTOR)
	{
		if(calibration_complete)
		{
			// Save calibration; exit calibration

			finish_calibration();
			
			save_calib();

			calibration_complete = false;
			testing = true;
		}
	}
	
	// Update the texture data.
	worker->begin();

	// Retrieve the data from the worker thread
	float* data = worker->get_data();

	// Preprocess the data so OpenCV can work with it
	// J.Leavell: TODO - This should probably be done on another thread its super slow
	uint8* cv_image = new uint8[worker->get_hm_width() * worker->get_hm_height()];
	for(int32 i = 0; i < worker->get_hm_width(); i++)
	{
		for(int32 j = 0; j < worker->get_hm_height(); j++)
		{
			int32 index = i + j * worker->get_hm_width();

			float value = 0;
			float data_pixel = data[index];
			
			if(frame_data.has_recorded)
			{
				float avg_frame_pixel = frame_data.avg_frame[index];

				// Only set value if read pixel is 5% closer to the camera than the background
				if (data_pixel > avg_frame_pixel * 1.01f)
				{
					value = 1.0f;
				}
			}
			else
			{
				// We haven't recorded an average frame, just use the current data pixel value
				value = data_pixel;
			}

			// Create a foreground frame for rendering
			foreground_frame[index] = value;
			
			cv_image[index] = (uint8) (value * 255);

//			if(cv_image[index] < 0 || cv_image[index] > 255)
//			{
//			}
		}
	}

	texture->data(foreground_frame, Ry::PixelFormat::RED, worker->get_hm_width(), worker->get_hm_height());

	// Pump data into OpenCV for blob detection
	cv::Mat im = cv::Mat_<uint8>(worker->get_hm_height(), worker->get_hm_width(), cv_image);

	std::vector<std::vector<cv::Point>> contours;
	cv::findContours(im, contours, cv::RetrievalModes::RETR_TREE, cv::ContourApproximationModes::CHAIN_APPROX_SIMPLE);

	cv::Moments m = cv::moments(im, true);
	cx = m.m10 / m.m00;
	cy = m.m01 / m.m00;

	record_frames(data);

	worker->end();
}

void CalibrateBox::handle_drag()
{
	if(dragging)
	{
		float sx = Ry::input_handler->getX();
		float sy = Ry::app->get_height() - Ry::input_handler->getY() - 1;

		rect_end.x = sx;
		rect_end.y = sy;
	}
}

void CalibrateBox::render()
{
	shape_batch->update(camera);
	texture_batch->update(camera);

	// Render the selected rectangle

	if(phase == CalibrationPhase::KINECT)
	{
		if (dragging || selected)
		{
			shape_batch->set_color(Ry::Vector4(0.0f, 0.0f, 1.0f, 1.0f));
			shape_batch->begin(Ry::DrawMode::FILLED);
			{
				shape_batch->draw_hollow_rect(rect_start.x, rect_start.y, rect_end.x - rect_start.x, rect_end.y - rect_start.y);
			}
			shape_batch->end();
		}
	}
	else if(phase == CalibrationPhase::PROJECTOR && !testing)
	{
		// Render crosshairs at current tiepoint
		shape_batch->set_color(Ry::Vector4(1.0f, 0.0f, 0.0f, 1.0f));
		shape_batch->begin(Ry::DrawMode::FILLED);
		{
			Ry::Vector2 CH = get_projector_space_point();

			// Draw horizontal line for vertical point
			shape_batch->draw_line(0, CH.y, Ry::app->get_width(), CH.y);

			// Draw vertical line for horizontal point
			shape_batch->draw_line(CH.x, 0, CH.x, Ry::app->get_height());
		}
		shape_batch->end();
	}

	// Render the blog rectangle that OpenCV gives us
	shape_batch->set_color(Ry::Vector4(0.0f, 1.0f, 0.0f, 1.0f));
	shape_batch->begin(Ry::DrawMode::FILLED);
	{

		// Convert from pixel coordinates to screen coordinates

		float scale_x = (float)Ry::app->get_width() / Kinect.w;
		float scale_y = (float)Ry::app->get_height() / Kinect.h;

		shape_batch->draw_hollow_circle(cx * scale_x, cy * scale_y, 5, 50);
	}
	shape_batch->end();

	if (testing)
	{
		// Render a crosshair at the middle of the object
		int32 depth_x = (int32)cx + Kinect.x;
		int32 depth_y = (int32)cy + Kinect.y;
		Ry::Vector3 camera_space = worker->get_camera_space(depth_x, depth_y);
		Ry::Vector4 homogenous = Ry::Vector4(camera_space.x, camera_space.y, camera_space.z, 1.0f);
		Ry::Vector4 projector_space = homography * homogenous;
		Ry::Vector2 screen_space = Ry::Vector2(projector_space.x, projector_space.y);
		Ry::Vector2 pixel_space = Ry::Vector2(screen_space.x * Ry::app->get_width() / 2 + Ry::app->get_width() / 2, -projector_space.y * Ry::app->get_height() / 2 + Ry::app->get_height() / 2);

		shape_batch->set_color(Ry::Vector4(0.0f, 1.0f, 0.0f, 1.0f));
		shape_batch->begin(Ry::DrawMode::FILLED);
		{
			// Draw horizontal line for vertical point
			shape_batch->draw_line(0, pixel_space.y, Ry::app->get_width(), pixel_space.y);

			// Draw vertical line for horizontal point
			shape_batch->draw_line(pixel_space.x, 0, pixel_space.x, Ry::app->get_height());
		}
		shape_batch->end();
	}

	// Render the kinect feed

	texture_batch->begin();
	texture_batch->draw_texture(texture, Ry::app->get_width() / 2, Ry::app->get_height() / 2, Ry::app->get_width(), Ry::app->get_height());
	texture_batch->end();

}

void CalibrateBox::quit()
{

}


void CalibrateBox::load_current()
{
	PlaneCalibration Plane;
	//load_calibration(projector_calib);
	load_calibration(Plane, Kinect, Proj);
	worker->set_bounds(Kinect.x, Kinect.y, Kinect.w, Kinect.h);

	homography = Proj.view_projection;

	testing = true;
	Ry::DesktopApp* desktop_app = dynamic_cast<Ry::DesktopApp*>(Ry::app);
	if (desktop_app)
	{
		desktop_app->set_fullscreen(true, 1);
		phase = CalibrationPhase::PROJECTOR;
	}
}

void CalibrateBox::finish_calibration()
{
	// OpenCV test
	std::vector<cv::Point3f> ObjectSpacePoints;
	std::vector<cv::Point2f> ProjectorSpacePoints;
	cv::Size CameraSize;
	cv::Mat CameraMatrix = cv::Mat::eye(3, 3, CV_64F);;
	cv::Mat DistCoeff = cv::Mat::zeros(8, 1, CV_64F);
	std::vector<cv::Mat> RValues;
	std::vector<cv::Mat> TValues;

	CameraSize.width = worker->get_hm_width();
	CameraSize.height = worker->get_hm_height();

	for(const TiePoint& Point : tie_points)
	{
		cv::Point3f ObjectSpacePoint;
		ObjectSpacePoint.x = Point.CameraSpace.x;
		ObjectSpacePoint.y = Point.CameraSpace.y;
		ObjectSpacePoint.z = Point.CameraSpace.z;

		ObjectSpacePoints.push_back(ObjectSpacePoint);

		cv::Point2f ProjectorSpacePoint;
		ProjectorSpacePoint.x = Point.ProjectorSpace.x;
		ProjectorSpacePoint.y = Point.ProjectorSpace.y;

		ProjectorSpacePoints.push_back(ProjectorSpacePoint);
	}

	cv::calibrateCamera(ObjectSpacePoints, ProjectorSpacePoints, CameraSize, CameraMatrix, DistCoeff, RValues, TValues);

	std::cout << "camera matrix: \n " << CameraMatrix << std::endl;

	std::cout << "r values" << std::endl;
	for(cv::Mat f : RValues)
	{
		std::cout << f << " ";
	}
	std::cout << std::endl;
	std::cout << "t values" << std::endl;
	for (cv::Mat t : TValues)
	{
		std::cout << t << " ";
	}
	std::cout << std::endl;

	// Calculate calibrated projection matrix

//	double* qr_solve(int m, int n, double a[], double b[]);
	// Solve first row

	// The number of rows is equal to the number of tie points

	homography = Ry::id4();

	int32 M = VERTICAL_POINTS * HORIZONTAL_POINTS;
	int32 N = 4;

	
	float z_far = -100000.0f;
	float z_near = 100000.0f;

	for(int32 mat_row = 0; mat_row < N; mat_row++)
	{

		double* mat = new double[M * N];
		double* b = new double[M];

		for (int32 i = 0; i < M; i++)
		{
			
			for (int32 j = 0; j < 3; j++)
			{
				mat[i + j * M] = tie_points.at(i).CameraSpace[j];
				std::cout << mat[i * N + j] << " ";
			}
			// W is always 1
			mat[i + 3 * M] = 1.0f;
			std::cout << mat[i * N + 3] << " ";

			switch(mat_row)
			{
			case 0:
				b[i] = tie_points.at(i).ProjectorSpace.x;
				break;
			case 1:
				b[i] = tie_points.at(i).ProjectorSpace.y;
				break;
			case 2:
				b[i] = 0.0f;
				break;
			case 3:
				b[i] = 1.0f;
				break;
			}

			std::cout << " * ? = " << b[i] << std::endl;

		}

		std::cout << std::endl;

		double* result = qr_solve(M, N, mat, b);
		homography[mat_row][0] = result[0];
		homography[mat_row][1] = result[1];
		homography[mat_row][2] = result[2];
		homography[mat_row][3] = result[3];
	}

	// Find z range
	for (const TiePoint& tie_point : tie_points)
	{
		if (tie_point.CameraSpace.z > z_far)
			z_far = tie_point.CameraSpace.z;
		if (tie_point.CameraSpace.z < z_near)
			z_near = tie_point.CameraSpace.z;
	}

	std::cout << "z near=" << z_near << " z far=" << z_far << std::endl;

	// Z row
	//homography[2][0] = 0.0f;
	//homography[2][1] = 0.0f;
	//homography[2][2] = -(z_far + z_near) / (z_far - z_near);
	//homography[2][3] = -(2 * z_far * z_near) / (z_far - z_near);
	
	// Fix last row
	homography[3][0] = 0.0f;
	homography[3][1] = 0.0f;
	homography[3][2] = 0.0f;
	homography[3][3] = 1.0f;

	/*
		m[2][0] = 0.0f;
		m[2][1] = 0.0f;
		m[2][2] = -(z_far + z_near) / (z_far - z_near);
		m[2][3] = -(2 * z_far * z_near) / (z_far - z_near);
	 */

	for (const TiePoint& tie_point : tie_points)
	{
		Ry::Vector4 result = homography * Ry::Vector4(tie_point.CameraSpace.x, tie_point.CameraSpace.y, tie_point.CameraSpace.z, 1.0f);

		std::cout << "result multiply " << *to_string(result) << " actual " << *to_string(tie_point.ProjectorSpace) << std::endl;

	}

	std::cout << *to_string(homography) << std::endl;
}

float CalibrateBox::get_drag_box_x()
{
	if (rect_end.x < rect_start.x)
		return rect_end.x;
	else
		return rect_start.x;
}

float CalibrateBox::get_drag_box_y()
{
	if (rect_end.y < rect_start.y)
		return rect_end.y;
	else
		return rect_start.y;
}

float CalibrateBox::get_drag_box_w()
{
	return std::abs(rect_end.x - rect_start.x);
}

float CalibrateBox::get_drag_box_h()
{
	return std::abs(rect_end.y - rect_start.y);
}

void CalibrateBox::record_frames(float* data)
{
	// Record frames
	if (frame_data.recording_frame)
	{
		for (int32 i = 0; i < WIDTH; i++)
		{
			for (int32 j = 0; j < HEIGHT; j++)
			{
				int32 index = j * WIDTH + i;
				frame_data.avg_frame[index] += data[j * WIDTH + i];

				if (frame_data.frame_to_record == AVG_COUNT - 1)
				{
					float val = frame_data.avg_frame[index];
					float avg_val = val / (float) AVG_COUNT;
					frame_data.avg_frame[index] = avg_val;
				}
			}
		}

		if(frame_data.frame_to_record % 10 == 0)
		{
			std::cout << "Frame " << frame_data.frame_to_record << " out of " << AVG_COUNT << " recorded" << std::endl;
		}

		frame_data.frame_to_record++;

		if (frame_data.frame_to_record >= AVG_COUNT)
		{
			frame_data.recording_frame = false;
			frame_data.has_recorded = true;
		}
	}
}

void CalibrateBox::process_avg()
{
	Ry::Vector3 avg_x;
	Ry::Vector3 avg_y;

	float x_step = 40.0f / (rect_end.x - rect_start.x);
	float y_step = 30.0f / -(rect_end.y - rect_start.y);

//	std::cout << "x_step: " << x_step << " y_step: " << y_step << std::endl;

	// J.Leavell: TODO - This is COMPLETELY wrong! The x and y coordinates from the kinect are a FUNCTION OF the x and y pixel values, not a direct correlation!
	for (int32 i = 0; i < WIDTH - 1; i++)
	{
		for (int32 j = 0; j < HEIGHT - 1; j++)
		{
			float ax = i * x_step;
			float ay = j * y_step;
			Ry::Vector3 a(ax, ay, frame_data.avg_frame[j*WIDTH+i]);
			Ry::Vector3 b(ax + x_step, ay, frame_data.avg_frame[j * WIDTH + i + 1]);
			Ry::Vector3 c(ax, ay + y_step, frame_data.avg_frame[(j+1) * WIDTH + i]);

	//		std::cout << i << " " << j << " " << *to_string(avg_x) << " " << *to_string(b) << " " << *to_string(a) << std::endl;
			
			avg_x += b - a;
			avg_y += c - a;
		}
	}

//	std::cout << "x: " << avg_x.x << " " << avg_x.y << " " << avg_x.z << std::endl;
//	std::cout << "y: " << avg_y.x << " " << avg_y.y << " " << avg_y.z << std::endl;

	avg_x *= 1.0f / ((WIDTH - 1) * (HEIGHT - 1));
	avg_y *= 1.0f / ((WIDTH - 1) * (HEIGHT - 1));

	normalize(avg_x);
	normalize(avg_y);

	Ry::Vector3 normal = cross(avg_x, avg_y);

//	std::cout << "Plane normal: " << normal.x << " " << normal.y << " " << normal.z << std::endl;
}

void CalibrateBox::save_calib()
{
	Ry::String bounds = Ry::to_string(Kinect.x) + " " + Ry::to_string(Kinect.y) + " " + Ry::to_string(Kinect.w) + " " + Ry::to_string(Kinect.h) + "\n";
	Ry::String projection = to_string(homography);
	Ry::String calib_string = bounds + projection;

	Ry::File::write_file("./calibration.dat", calib_string);
	std::cout << "Successfully saved calibration to calibration.dat" << std::endl;
}