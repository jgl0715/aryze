#version 430 core

#define LINES 40

// Render to color attachment 0
layout (location = 0) out vec4 frag_color;

in float height;

void main()
{
	// How many topography lines
	float value = height;
	float scaled_value = value * LINES;	
	int f_i = int(floor(scaled_value));

	vec4 elevation_colors[LINES];
	elevation_colors[0] = vec4(0.1765,0.4980,0.5804, 1.0);
	elevation_colors[1] = vec4(0.1608,0.5294,0.6314, 1.0);
	elevation_colors[2] = vec4(0.2157,0.5725,0.6863, 1.0);
	elevation_colors[3] = vec4(0.2745,0.6039,0.7451, 1.0);
	elevation_colors[4] = vec4(0.3725,0.6627,0.8000, 1.0);
	elevation_colors[5] = vec4(0.4510,0.7020,0.8118, 1.0);
	elevation_colors[6] = vec4(0.5608,0.7647,0.8510, 1.0);
	elevation_colors[7] = vec4(0.6667,0.8157,0.8588, 1.0);
	elevation_colors[8] = vec4(0.7765,0.8980,0.9059, 1.0);
	elevation_colors[9] = vec4(0.8863,0.9412,0.9451, 1.0);
	elevation_colors[10] = vec4(0.6902,0.9490,0.8039, 1.0);
	elevation_colors[11] = vec4(0.7059,0.9647,0.7020, 1.0);
	elevation_colors[12] = vec4(0.8118,0.9804,0.6941, 1.0);
	elevation_colors[13] = vec4(0.9490,0.9882,0.7020, 1.0);
	elevation_colors[14] = vec4(0.8627,0.9333,0.5686, 1.0);
	elevation_colors[15] = vec4(0.5765,0.8157,0.3647, 1.0);
	elevation_colors[16] = vec4(0.2706,0.7020,0.2078, 1.0);
	elevation_colors[17] = vec4(0.0824,0.5922,0.1843, 1.0);
	elevation_colors[18] = vec4(0.0706,0.5098,0.2471, 1.0);
	elevation_colors[19] = vec4(0.3176,0.5647,0.2275, 1.0);
	elevation_colors[20] = vec4(0.5176,0.6196,0.1843, 1.0);
	elevation_colors[21] = vec4(0.7098,0.6706,0.1373, 1.0);
	elevation_colors[22] = vec4(0.9137,0.7098,0.0667, 1.0);
	elevation_colors[23] = vec4(0.9216,0.5843,0.0078, 1.0);
	elevation_colors[24] = vec4(0.8196,0.3804,0.0078, 1.0);
	elevation_colors[25] = vec4(0.6941,0.2118,0.0078, 1.0);
	elevation_colors[26] = vec4(0.5804,0.0784,0.0039, 1.0);
	elevation_colors[27] = vec4(0.4863,0.0235,0.0039, 1.0);
	elevation_colors[28] = vec4(0.4588,0.0824,0.0157, 1.0);
	elevation_colors[29] = vec4(0.4431,0.1176,0.0235, 1.0);
	elevation_colors[30] = vec4(0.4275,0.1529,0.0353, 1.0);
	elevation_colors[31] = vec4(0.4157,0.1765,0.0471, 1.0);
	elevation_colors[32] = vec4(0.4588,0.2510,0.1176, 1.0);
	elevation_colors[33] = vec4(0.5216,0.3451,0.2353, 1.0);
	elevation_colors[34] = vec4(0.5843,0.4431,0.3647, 1.0);
	elevation_colors[35] = vec4(0.6353,0.5647,0.5294, 1.0);
	elevation_colors[36] = vec4(0.6784,0.6784,0.6745, 1.0);
	elevation_colors[37] = vec4(0.7451,0.7451,0.7451, 1.0);
	elevation_colors[38] = vec4(0.8235,0.8157,0.8235, 1.0);
	elevation_colors[39] = vec4(0.8902,0.8824,0.8902, 1.0);

	frag_color = normalize(elevation_colors[f_i]) * 3.0;
}