#version 430 core

out vec4 frag_color;

in vec2 tex_coord;
in vec4 vert_color;

void main()
{
	frag_color = vert_color;
}