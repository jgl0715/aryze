#version 430 core

uniform sampler2D u_sampler;
uniform float width;
uniform float height;

layout (location = 0) out vec4 frag_color;

in vec2 tex_coord;

void main()
{
	float sample_x = width * tex_coord.x;
	float sample_y = height * tex_coord.y;

	vec4 mid_color = texelFetch(u_sampler, ivec2(sample_x, sample_y), 0);
	int is_line = 0;

	frag_color = mid_color;
	
	for(int i = 0; i < 3; i++)
	{
		for(int j = 0; j < 3; j++)
		{
			ivec2 actual = ivec2(sample_x + (i - 1), sample_y + (j - 1));
			vec4 test_color = texelFetch(u_sampler, actual, 0);

			if(test_color != mid_color && i != j)
				frag_color = vec4(0.0f, 0.0f, 0.0f, 1.0f);
		}
	}

}