#version 130

uniform vec3 diffuse_color;

varying vec2 tex_coord;
varying vec4 vert_color;

void main()
{
	gl_FragColor = vert_color * vec4(diffuse_color, 1.0);
}