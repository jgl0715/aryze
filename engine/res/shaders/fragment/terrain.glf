#version 430 core

layout (location = 2) uniform sampler2D u_sampler;
layout (location = 3) uniform float u_width;
layout (location = 4) uniform float u_height;
//layout (location = 3) uniform float u_screen_width;
//layout (location = 4) uniform float u_screen_height;

out vec4 frag_color;

in vec2 tex_coord;
in vec4 vert_color;

void main()
{
	
//    float value = texture(u_sampler, tex_coord).r;

	float texel_x = tex_coord.x * u_width;
	float texel_y = tex_coord.y * u_height;
	ivec2 texel = ivec2(texel_x, texel_y);

	float values[9];
	for(int i = 0; i < 3; i++)
	{
		for(int j = 0; j < 3; j++)
		{
			// Insertion
			values[i*3+j] = texelFetch(u_sampler, texel + ivec2(i-1,j-1), 0).r;		
		}
	}

	// How many topography lines
	int lines = 9;

	// Middle value (median)
	float value = values[4];
	float scaled_value = value * lines;
	float f_f = floor(scaled_value);
	int f_i = int(f_f);
	float is_not_line = 1;

	for(int i = 0; i < 3; i++)
	{
		for(int j = 0; j < 3; j++)
		{
			float v = floor(values[i*3+j] * lines); 
			if(i != j && v != f_f)
			{
				is_not_line = 0;
			}
		}
	}
	
	vec4 elevation_colors[9];
	elevation_colors[0] = vec4(0.0, 0.0, 1.0, 1.0);
	elevation_colors[1] = vec4(0.004, 0.196, 0.125, 1.0);
	elevation_colors[2] = vec4(0.75, 1.0, 0.0, 1.0);
	elevation_colors[3] = vec4(1.0, 1.0, 0.0, 1.0);
	elevation_colors[4] = vec4(1.0, 0.5, 0.0, 1.0);
	elevation_colors[5] = vec4(0.588, 0.294, 0.0, 1.0);
	elevation_colors[6] = vec4(0.396, 0.263, 0.129, 1.0);
	elevation_colors[7] = vec4(0.8, 0.8, 0.8, 1.0);
	elevation_colors[8] = vec4(1.0, 1.0, 1.0, 1.0);

	vec4 line_colors[9];
	line_colors[0] = vec4(1.0, 1.0, 1.0, 1.0);
	line_colors[1] = vec4(0.0, 0.0, 0.0, 0.0);
	line_colors[2] = vec4(0.0, 0.0, 0.0, 0.0);
	line_colors[3] = vec4(0.0, 0.0, 0.0, 0.0);
	line_colors[4] = vec4(0.0, 0.0, 0.0, 0.0);
	line_colors[5] = vec4(0.0, 0.0, 0.0, 0.0);
	line_colors[6] = vec4(0.0, 0.0, 0.0, 0.0);
	line_colors[7] = vec4(0.0, 0.0, 0.0, 0.0);
	line_colors[8] = vec4(1.0, 1.0, 1.0, 1.0);


	frag_color = elevation_colors[f_i] * is_not_line;
}