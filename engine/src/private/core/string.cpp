#include "core/string.h"
#include <string>
#include <iostream>
#include <vector>
#include <cstdlib>

namespace Ry
{

	String to_string(int64 a)
	{
		return String(std::to_string(a).c_str());
	}

	String to_string(uint64 a)
	{
		return String(std::to_string(a).c_str());
	}

	String to_string(uint32 a)
	{
		return String(std::to_string(a).c_str());
	}

	String to_string(int32 a)
	{
		return String(std::to_string(a).c_str());
	}

	String to_string(double a)
	{
		return String(std::to_string(a).c_str());
	}

	String to_string(float a)
	{
		return String(std::to_string(a).c_str());
	}

	String to_string(bool a)
	{
		return String(std::to_string(a).c_str());
	}

	String::String()
	{
		data = new char[1];
		data[0] = '\0';

		size = 0;
	}

	String::String(const char* dat)
	{
		size = 0;
		while (dat[size])
			size++;

		data = new char[size + 1];

		strcpy_s(data, size + 1, dat);
	}

	String::String(const String& other)
	{
		size = other.size;
		data = new char[size + 1];
		for (uint32 i = 0; i < size + 1; i++)
			data[i] = other.data[i];
	}

	String::~String()
	{
		delete data;
	}

	const char* String::getData() const
	{
		return data;
	}

	uint32 String::getSize() const
	{
		return size;
	}

	String String::to_lower() const
	{
		char* dst = new char[size + 1];

		for (int i = 0; i < size; i++)
		{
			dst[i] = tolower((int)data[i]);
		}

		dst[size] = '\0';

		return String(dst);
	}

	void String::operator=(const String& other)
	{
		if (data)
			delete data;
		size = other.size;
		data = new char[size + 1];

		// This also copies over the null terminator
		for (uint32 i = 0; i < size + 1; i++)
			data[i] = other.data[i];
	}

	bool String::operator==(const String& other) const
	{
		if (size != other.size)
			return false;

		return this->operator==(other.data);
	}

	bool String::operator==(const char* other) const
	{

		if (size != strlen(other))
			return false;

		if (size == 0)
		{
			if (other[0] == '\0')
				return true;
			return false;
		}

		for (uint32 i = 0; i < size; i++)
		{
			if (data[i] != other[i])
				return false;
		}

		return true;
	}

	bool String::operator!=(const String& other) const
	{
		return !(*this == other);
	}

	bool String::operator!=(const char* other) const
	{
		return !(*this == other);
	}

	uint32 String::operator()() const
	{
		const int32 p = 31;
		const int32 m = 1e9 + 9;
		uint32 hash_value = 0;
		uint32 p_pow = 1;

		for (int32 i = 0; i < size; i++)
		{
			char c = data[i];
			hash_value = (hash_value + (c - 'a' + 1) * p_pow) % m;
			p_pow = (p_pow * p) % m;
		}

		return hash_value;
	}

	String String::operator+(const String& other) const
	{
		char* newData = new char[size + other.size + 1];
		for (uint32 i = 0; i < size; i++)
			newData[i] = data[i];
		for (uint32 i = size; i < size + other.size; i++)
			newData[i] = other.data[i - size];
		newData[size + other.size] = '\0';

		return String(newData);
	}

	String& String::operator+=(const String& other)
	{
		const_cast<String*>(this)->operator=(*this + other);
		
		return *this;
	}

	double String::to_double() const
	{
		return std::stod(data);
	}

	float String::to_float() const
	{
		return std::stof(data);
	}
	
	int32 String::to_int32() const
	{
		return std::stoi(data);
	}
	
	uint32 String::to_uint32() const
	{
		return (uint32) std::stoul(data);
	}
	
	bool String::to_bool() const
	{
		if (*this == "true")
			return true;
		else if (*this == "false")
			return false;
		else if (*this == "True")
			return true;
		else if (*this == "False")
			return false;
		else if (*this == "TRUE")
			return true;
		else if (*this == "FALSE")
			return false;
		else if (*this == "1")
			return true;
		else if (*this == "0")
			return false;
		else if (*this == "t")
			return true;
		else if (*this == "f")
			return false;
		else if (*this == "t")
			return true;
		else if (*this == "f")
			return false;

		return false;
	}

	String operator+(const char* a, const String& b)
	{
		return String(a) + b;
	}

	char String::operator[](uint32 index) const
	{
		return data[index];
	}

	String String::operator+(const char* other) const
	{
		size_t otherSize = strlen(other);
		char* newData = new char[size + otherSize + 1];

		for (uint32 i = 0; i < size; i++)
			newData[i] = data[i];

		for (uint32 i = size; i < size + otherSize; i++)
			newData[i] = other[i - size];

		newData[size + otherSize] = '\0';

		return String(newData);
	}

	const char* String::operator*() const
	{
		return data;
	}

	int32 String::split(const String& str, String** result)
	{
		CORE_ASSERT(str.getSize() > 0, "String::split size of delimiter must be greater than zero");

		if (getSize() == 0)
		{
			*result = nullptr;
			return 0;
		}

		// There can not be more delimiters found than the size of the string (unless, of course, we allow empty strings which we're not)
		// Also, we're allocating the delimiters on the stack for small strings for efficiency
		uint32* delims = new uint32[getSize() + 1];

		// Set the first delimited index as the beginning of the string
		uint32 delim = 0;

		// i represents the end of the last delimiter
		uint32 i = 0;

		// The length of the delimiter
		const int32 LEN = str.getSize();

		while (i < getSize())
		{
			int32 next = find_first(str, i);

			delims[delim] = i;

			if (next > 0)
			{
				delims[delim + 1] = next;
				i = next + LEN;
			}
			else
			{
				// Once we can't find any more delimiters, the last delimited index is the end of the string.
				delims[delim + 1] = (i = getSize());
			}

			delim += 2;
		}

		int32 strings = delim / 2;
		*result = new String[strings];


		for (uint32 i = 0, j = 0; i < delim - 1; i += 2, j++)
		{
			uint32 this_delim = delims[i];
			uint32 next_delim = delims[i + 1];

			(*result)[j] = substring(this_delim, next_delim);
		}

		delete delims;

		return delim / 2;
	}

	String String::substring(uint32 beg, uint32 end) const
	{
		CORE_ASSERT(beg >= 0, "String::substring beg must be greater than or equal to zero");
		CORE_ASSERT(beg < getSize(), "String::substring beg must be less than string size");
		CORE_ASSERT(end >= 0, "String::substring end must be greater than or equal to zero");
		CORE_ASSERT(end <= getSize(), "String::substring end must be less than or equal to string size");
		CORE_ASSERT(beg <= end, "String::substring beg must be less than or equal to end");

		char* chars = new char[end - beg + 1];

		for (uint32 i = beg; i < end; i++)
		{
			chars[i - beg] = data[i];
		}
		chars[end - beg] = '\0';

		return String(chars);
	}

	String String::right_most(uint32 count) const
	{
		CORE_ASSERT(count <= getSize(), "right_most count must be less than or equal to string size");

		if (count == getSize())
			return *this;

		char* chars = new char[count + 1];
		for (uint32 i = 0; i < count; i++)
			chars[i] = data[i + (getSize() - count)];
		chars[count] = '\0';

		return String(chars);
	}

	String String::left_most(uint32 count) const
	{
		CORE_ASSERT(count <= getSize(), "left_most count must be less than or equal to string size");

		if (count == getSize())
			return *this;

		char* chars = new char[count + 1];
		for (uint32 i = 0; i < count; i++)
			chars[i] = data[i];
		chars[count] = '\0';

		return String(chars);
	}

	int32 String::find_first(const String& str, uint32 index) const
	{
		CORE_ASSERT(index >= 0, "find_first index must be greater than or equal to zero");
		CORE_ASSERT(index < getSize(), "find_first index must be less than string size");

		/************************************************************************/
		/* Shortcuts                                                            */
		/************************************************************************/

		// The first empty string is always at the beginning of any string
		if (str.getSize() < 1)
			return index;

		// Substring must be smaller for it to occur in this string
		if (str.getSize() > getSize())
			return -1;

		// Check if there is enough space for the substring to possibly be in this string
		if (getSize() - index < str.getSize())
			return -1;


		uint32 i = index;
		uint32 j = 0;
		uint32 at = i;
		int32 found = -1;

		while (i < getSize() && j < str.getSize() && found < 0)
		{
			if (data[i] == str[j])
			{
				if (j == 0)
				{
					at = i;
				}

				j++;

				if (j == str.getSize())
				{
					found = at;
				}
			}
			else
			{
				j = 0;
				at = i;
			}

			i++;
		}

		return found;
	}

	int32 String::find_last(const String& str, uint32 index) const
	{
		CORE_ASSERT(index >= 0, "find_last index must be greater than or equal to zero");
		CORE_ASSERT(index < getSize(), "find_last index must be less than string size");

		/************************************************************************/
		/* Shortcuts                                                            */
		/************************************************************************/

		// The first empty string is always at the beginning of any string
		if (str.getSize() < 1)
			return index;

		// Substring must be smaller for it to occur in this string
		if (str.getSize() > getSize())
			return -1;

		// Check if there is enough space for the substring to possibly be in this string
		if (getSize() - index - 1 < str.getSize())
			return -1;

		int32 i = getSize() - index - 1;
		int32 j = str.getSize() - 1;
		int32 found = -1;

		while (i >= 0 && j >= 0 && found < 0)
		{
			if (data[i] == str[j])
			{
				j--;

				if (j < 0)
				{
					found = i;
				}
			}
			else
			{
				j = str.getSize() - 1;
			}

			i--;
		}

		return found;
	}
}

void FreeArray(Ry::String* arrayPtr)
{
	delete[] arrayPtr;
}