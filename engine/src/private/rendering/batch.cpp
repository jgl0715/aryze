#include "rendering/2d/batch.h"
#include "rendering/mesh.h"
#include "rendering/camera.h"
#include "globals.h"
#include <iostream>
#include <cmath>
#include "math/math.h"

namespace Ry
{
	
	ShapeBatch::ShapeBatch()
	{
		shader = rapi->make_shader(VF1P1UV1C, SHAPE_VERT, SHAPE_FRAG);
		mesh = new Ry::Mesh(VF1P1C, BufferHint::DYNAMIC);
		color = Ry::Vector4(1.0f, 1.0f, 1.0f, 1.0f);
		mesh->set_shader(0, shader);
		prim = Primitive::TRIANGLE;
		began = false;
	
		view = id4();
		proj = id4();
	}
	
	ShapeBatch::~ShapeBatch()
	{
		delete shader;
		delete mesh;
	}
	
	void ShapeBatch::begin(DrawMode mode)
	{
		CHECK_ENDED()
	
		this->mode = mode;
		this->began = true;
	}
	
	void ShapeBatch::draw_hollow_circle(float x, float y, float r, uint32 segments)
	{
		CHECK_BEGAN()
		CHECK_FLUSH_PRIM(Primitive::LINE, segments, 2 * segments);
	
		if (segments < 3)
		{
			std::cerr << "must have at least 3 segments for a triangle" << std::endl;
			return;
		}
	
		uint32 i0 = mesh->get_vert_count();
	
		float angle_inc = (2 * PI) / segments;
		for (uint32 i = 0; i < segments; i++)
		{
			float x_o = x + cos(angle_inc * i) * r;
			float y_o = y + sin(angle_inc * i) * r;
			Vertex1P1UV1C vert(x_o, y_o, 0.0f, 0.0f, 0.0f, color.r, color.g, color.b, color.a);
			mesh->add_vertex(&vert);
	
			if (i < segments - 1)
			{
				mesh->add_index(i0 + i);
				mesh->add_index(i0 + i + 1);
			}
		}
		mesh->add_index(i0 + segments - 1);
		mesh->add_index(i0);
	
	}
	
	void ShapeBatch::draw_filled_circle(float x, float y, float r, uint32 segments)
	{
		CHECK_BEGAN()
		CHECK_FLUSH_PRIM(Primitive::TRIANGLE, segments + 1, 3 * segments);
	
		if (segments < 3)
		{
			std::cerr << "must have at least 3 segments for a triangle" << std::endl;
			return;
		}
	
		uint32 i0 = mesh->get_vert_count();
	
		// Center vertex.
		Vertex1P1UV1C center(x, y, 0.0f, 0.0f, 0.0f, color.r, color.g, color.b, color.a);
		mesh->add_vertex(&center);
	
		float angle_inc = (2 * PI) / segments;
		for (uint32 i = 0; i < segments; i++)
		{
			float x_o = x + cos(angle_inc * i) * r;
			float y_o = y + sin(angle_inc * i) * r;
	
			Vertex1P1UV1C vert(x_o, y_o, 0.0f, 0.0f, 0.0f, color.r, color.g, color.b, color.a);
			mesh->add_vertex(&vert);
	
			if (i < segments - 1)
			{
				mesh->add_tri(i0, i0 + 1 + i, i0 + 2 + i);
			}
		}
		mesh->add_tri(i0, i0 + segments, i0 + 1);
	}
	
	void ShapeBatch::draw_rect(float x, float y, float w, float h)
	{
		CHECK_BEGAN()
		CHECK_FLUSH_PRIM(Primitive::TRIANGLE, 4, 6)
	
		Vertex1P1UV1C v1(x, y, 0.0f, 0.0f, 0.0f, color.r, color.g, color.b, color.a);
		Vertex1P1UV1C v2(x, y + h, 0.0f, 0.0f, 0.0f, color.r, color.g, color.b, color.a);
		Vertex1P1UV1C v3(x + w, y + h, 0.0f, 0.0f, 0.0f, color.r, color.g, color.b, color.a);
		Vertex1P1UV1C v4(x + w, y, 0.0f, 0.0f, 0.0f, color.r, color.g, color.b, color.a);
	
		ADD_QUAD(v1, v2, v3, v4);
	}
	
	void ShapeBatch::draw_hollow_rect(float x, float y, float w, float h)
	{
		CHECK_BEGAN()
		CHECK_FLUSH_PRIM(Primitive::LINE, 4, 6)
	
		Vertex1P1UV1C v1(x, y, 0.0f, 0.0f, 0.0f, color.r, color.g, color.b, color.a);
		Vertex1P1UV1C v2(x, y + h, 0.0f, 0.0f, 0.0f, color.r, color.g, color.b, color.a);
		Vertex1P1UV1C v3(x + w, y + h, 0.0f, 0.0f, 0.0f, color.r, color.g, color.b, color.a);
		Vertex1P1UV1C v4(x + w, y, 0.0f, 0.0f, 0.0f, color.r, color.g, color.b, color.a);
	
		uint32 i0 = mesh->get_vert_count();
		mesh->add_vertex(&v1);
		mesh->add_vertex(&v2);
		mesh->add_vertex(&v3);
		mesh->add_vertex(&v4);
		mesh->add_line(i0, i0 + 1);
		mesh->add_line(i0 + 1, i0 + 2);
		mesh->add_line(i0 + 2, i0 + 3);
		mesh->add_line(i0 + 3, i0 + 0);
	}
	
	void ShapeBatch::draw_line(float x1, float y1, float x2, float y2)
	{
		CHECK_BEGAN()
		CHECK_FLUSH_PRIM(Primitive::LINE, 2, 2)
	
		Vertex1P1UV1C v1(x1, y1, 0.0f, 0.0f, 0.0f, color.r, color.g, color.b, color.a);
		Vertex1P1UV1C v2(x2, y2, 0.0f, 0.0f, 0.0f, color.r, color.g, color.b, color.a);
	
		ADD_LINE(v1, v2);
	}
	
	void ShapeBatch::flush()
	{
		if (mesh->get_vert_count() > 0)
		{
			shader->uniformMat44("view_proj", proj * view);
			shader->uniformMat44("model", id4());

			mesh->update();
			mesh->render(prim);
			mesh->clear();
		}
	}
	
	void ShapeBatch::end()
	{
		flush();
		began = false;
	}
	
	void ShapeBatch::set_color(const Ry::Vector4& color)
	{
		this->color = color;
	}
	
	void ShapeBatch::set_projection(const Matrix4& proj)
	{
		this->proj = proj;
	}
	
	void ShapeBatch::set_view(const Matrix4& view)
	{
		this->view = view;
	}
	
	void ShapeBatch::update(const Camera* cam)
	{
		proj = cam->get_proj();
		view = cam->get_view();
	}
	
	TextureBatch::TextureBatch()
	{
		shader = rapi->make_shader(VF1P1UV1C, TEX_VERT, TEX_FRAG);
		mesh = new Mesh(VF1P1UV1C, BufferHint::DYNAMIC);
		color = Ry::Vector4(1.0f, 1.0f, 1.0f, 1.0f);
		mesh->set_shader(0, shader);
		began = false;
	
		view = id4();
		proj = id4();
		texture = 0;
	}
	
	TextureBatch::~TextureBatch()
	{
		delete shader;
		delete mesh;
	}
	
	void TextureBatch::begin()
	{
		CHECK_ENDED()
	
		this->began = true;
	}
	
	void TextureBatch::draw_texture(Texture* texture, float x, float y, float w, float h)
	{
		CHECK_BEGAN()
		CHECK_FLUSH_TEXTURE(texture)

		float hw = w / 2;
		float hh = h / 2;
	
		Vertex1P1UV1C v1(x + -hw, y + -hh, 0.0f, 0.0f, 1.0f, color.r, color.g, color.b, color.a);
		Vertex1P1UV1C v2(x + -hw, y + hh, 0.0f, 0.0f, 0.0f, color.r, color.g, color.b, color.a);
		Vertex1P1UV1C v3(x + hw, y + hh, 0.0f, 1.0f, 0.0f, color.r, color.g, color.b, color.a);
		Vertex1P1UV1C v4(x + hw, y + -hh, 0.0f, 1.0f, 1.0f, color.r, color.g, color.b, color.a);
	
		ADD_QUAD(v1, v2, v3, v4);
	}
	
	void TextureBatch::flush()
	{
		if (mesh->get_vert_count() > 0)
		{
			texture->bind();
//			glBindTexture(GL_TEXTURE_2D, texture); 
			{
				shader->uniformMat44("view_proj", proj * view);
				shader->uniformMat44("model", id4());
	
				// Update the mesh data for the new frame
				mesh->update();

				// Render the new frame
				mesh->render(Primitive::TRIANGLE);
				
				// Clear the data for this frame in preparation for next frame
				mesh->clear();
			}
			texture->unbind();
//			glBindTexture(GL_TEXTURE_2D, 0);
		}
	}
	
	void TextureBatch::end()
	{
		flush();
	
		began = false;
	}
	
	void TextureBatch::set_shader(Shader* shader)
	{
		this->shader = shader;
	}
	
	void TextureBatch::set_color(const Ry::Vector4& color)
	{
		this->color = color;
	}
	
	void TextureBatch::set_projection(const Matrix4& proj)
	{
		this->proj = proj;
	}
	
	void TextureBatch::set_view(const Matrix4& view)
	{
		this->view = view;
	}
	
	void TextureBatch::update(const Camera* cam)
	{
		proj = cam->get_proj();
		view = cam->get_view();
	}
}