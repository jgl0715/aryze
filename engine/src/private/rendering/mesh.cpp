#include "rendering/mesh.h"
#include "rendering/interface/shader.h"
#include "rendering/interface/vertexarray.h"
#include "globals.h"
#include "util.h"
#include "file/file.h"
#include <iostream>

namespace Ry
{
	
	Mesh::Mesh(BufferHint hint):
		Mesh(VF1P, hint)
	{
	
	}
	
	Mesh::Mesh(const VertexFormat& format, BufferHint usage)
	{
		this->hint = usage;

		this->vao = rapi->make_vertex_array(format, true);
		this->max_section_id = 0;
	
		vert_count = 0;
		index_count = 0;

		// Create default section 0
		new_section();
	}
	
	Mesh::~Mesh()
	{
		delete vao;	
	}
	
	void Mesh::deleteMesh()
	{
		vao->deleteArray();
	}
	
	uint32 Mesh::get_vert_count() const
	{
		return vert_count;
	}
	
	uint32 Mesh::get_index_count() const
	{
		return index_count;
	}

	void Mesh::new_section()
	{
		MeshSection new_section;
		new_section.section_id = max_section_id;
		new_section.start_index = max_section_id > 0 ? sections[max_section_id - 1].start_index + sections[max_section_id - 1].count : 0;
		new_section.count = 0;
		new_section.shader = nullptr;
		new_section.material = nullptr;

		sections[max_section_id] = new_section;
		max_section_id++;
	}

	Material* Mesh::get_material(int32 slot)
	{
		return sections[slot].material;
	}
	
	void Mesh::set_material(int32 slot, Material* mat)
	{
		sections[slot].material = mat;
	}

	Shader* Mesh::get_shader(int32 slot)
	{
		return sections[slot].shader;
	}

	void Mesh::set_shader_all(Shader* shader)
	{
		std::unordered_map<int32, MeshSection>::iterator mat_itr = sections.begin();

		while (mat_itr != sections.end())
		{
			mat_itr->second.shader = shader;
			++mat_itr;
		}
	}
	
	void Mesh::set_shader(int32 slot, Shader* shader)
	{
		sections[slot].shader = shader;
	}

	int32 Mesh::get_current_section_index() const
	{
		return max_section_id - 1;
	}
	
	void Mesh::add_vertex(const Vertex* vert)
	{
		// ASSUMPTION: If you have over 30 elements in your vertex you're doing something wrong.
		float data[30];
		vert->populate(data);
		
		for (int32 i = 0; i < vao->get_format().element_count; i++)
		{
			verts.push_back(data[i]);
		}

		vert_count++;
	}

	void Mesh::add_vertex(float x, float y, float z, float u, float v, float r, float g, float b, float a)
	{
		verts.push_back(x);
		verts.push_back(y);
		verts.push_back(z);
		verts.push_back(u);
		verts.push_back(v);
		verts.push_back(r);
		verts.push_back(g);
		verts.push_back(b);
		verts.push_back(a);

		vert_count++;
	}

	void Mesh::add_vertex(float* data)
	{
		int32 data_index = 0;
	
		for (int32 i = 0; i < vao->get_format().attribute_count; i++)
		{
			VertexAttrib& attrib = vao->get_format().attributes[i];
			int32 attrib_size = attrib.size;
	
			for (int32 j = 0; j < attrib_size; j++)
			{
				verts.push_back(data[data_index + j]);
			}
	
			data_index += attrib_size;
		}

		vert_count++;
	}
	
	void Mesh::add_index(uint32 index)
	{
		indices.push_back(index);

		index_count++;
		sections[max_section_id - 1].count++;
	}
	
	void Mesh::add_line(uint32 i1, uint32 i2)
	{
		indices.push_back(i1);
		indices.push_back(i2);

		index_count += 2;
		sections[max_section_id - 1].count += 2;
	}
	
	void Mesh::add_tri(uint32 i1, uint32 i2, uint32 i3)
	{
		indices.push_back(i1);
		indices.push_back(i2);
		indices.push_back(i3);

		index_count += 3;
		sections[max_section_id - 1].count += 3;
	}
	
	void Mesh::update()
	{
		vao->push_vert_data(verts.data(), vert_count, hint);
		vao->push_index_data(indices.data(), index_count, hint);
	}
	
	void Mesh::render(Primitive prim)
	{
		std::unordered_map<int32, MeshSection>::iterator mat_itr = sections.begin();

		while (mat_itr != sections.end())
		{
			MeshSection section = mat_itr->second;
			Material* mat = section.material;
			Shader* shader = section.shader;
			int32 start_index = section.start_index;
			int32 count = section.count;


			shader->bind();
			{
				// Set shader material uniforms
				if (mat)
				{
					//shader->uniform_vec3("ambient_color", mat->ambient_color);
					shader->uniform_vec3("diffuse_color", mat->diffuse_color);
					//shader->uniform_vec3("specular_color", mat->specular_color);
				}

				vao->render(prim, start_index, count);
			}
			shader->unbind();

			++mat_itr;
		}

	}
	
	void Mesh::clear()
	{
		// TODO: Performance: could probably be better here, do not erase elements but stomp over them instead maintaining the greatest allocated size
		verts.clear();
		indices.clear();

		vert_count = 0;
		index_count = 0;
	}

	Mesh* load_obj(const Ry::String mesh_path, const Ry::String material_path)
	{
		Mesh* result = new Mesh(VF1P1UV1C, BufferHint::STATIC);
		std::vector<Ry::Vector3> positions;
		std::vector<int32> indices;

		String* mesh_lines;
		String* material_lines;
		Ry::String mesh_contents = Ry::File::load_file_as_string(mesh_path);
		Ry::String material_contents = Ry::File::load_file_as_string(material_path);
		int32 mesh_line_count = mesh_contents.split("\n", &mesh_lines);
		int32 material_line_count = material_contents.split("\n", &material_lines);

		std::unordered_map<std::string, Material*> materials;

		Material* current_mat = nullptr;

		// Load the OBJ materials
		for (int32 i = 0; i < material_line_count; i++)
		{
			String* tokens;
			String line = material_lines[i];
			int32 token_count = line.split(" ", &tokens);

			if (token_count > 0)
			{
				if (tokens[0] == "newmtl")
				{
					if (current_mat)
					{
						// TODO: Decide material shader here

						materials[std::string(*current_mat->name)] = current_mat;
					}

					current_mat = new Material;
					current_mat->name = tokens[1];
				}

				if (current_mat)
				{
					if (tokens[0] == "Ka")
					{
						current_mat->ambient_color.r = tokens[1].to_float();
						current_mat->ambient_color.g = tokens[2].to_float();
						current_mat->ambient_color.b = tokens[3].to_float();
					}
					if (tokens[0] == "Kd")
					{
						current_mat->diffuse_color.r = tokens[1].to_float();
						current_mat->diffuse_color.g = tokens[2].to_float();
						current_mat->diffuse_color.b = tokens[3].to_float();
					}
					if (tokens[0] == "Ks")
					{
						current_mat->specular_color.r = tokens[1].to_float();
						current_mat->specular_color.g = tokens[2].to_float();
						current_mat->specular_color.b = tokens[3].to_float();
					}
				}
			}

		}

		bool skipped_first = false;

		// Load the OBJ mesh data
		for (int32 i = 0; i < mesh_line_count; i++)
		{
			String* tokens;
			String line = mesh_lines[i];
			int32 token_count = line.split(" ", &tokens);
			
			if (token_count > 0)
			{

				// Create a new section for the group.
				// We do not care about the OBJ name of this group, as mesh sections are just identified by generated id number.
				if (tokens[0] == "g")
				{

					if (skipped_first)
					{

						// Flush out the indices
						for (int32 index : indices)
						{
							result->add_index(index);
						}
						indices.clear();

						result->new_section();
					}
					else
					{
						skipped_first = true;
					}
				}

				// Set the material of the current section
				if (tokens[0] == "usemtl")
				{
					int32 mat_index = result->get_current_section_index();
					Ry::String mat_name = tokens[1];
					result->set_material(mat_index, materials[std::string(*mat_name)]);
				}

				if (tokens[0] == "v")
				{
					// Triangle primitives
					if (token_count == 4)
					{
						float x = tokens[1].to_float();
						float y = tokens[2].to_float();
						float z = tokens[3].to_float();
						positions.push_back(Ry::Vector3(x, y, z));
					}

					// Rectangle primitives
					if (token_count == 5)
					{
						std::cerr << "OBJ mesh loader only supports triangluated meshes" << std::endl;
						return nullptr;
					}
				}

				if (tokens[0] == "f")
				{

					if (token_count == 4)
					{
						int32 i1 = tokens[1].to_int32() - 1;
						int32 i2 = tokens[2].to_int32() - 1;
						int32 i3 = tokens[3].to_int32() - 1;

						indices.push_back(i1);
						indices.push_back(i2);
						indices.push_back(i3);
					}

					if (token_count == 5)
					{
						std::cerr << "OBJ mesh loader only supports triangluated meshes" << std::endl;
						return nullptr;
					}
				}

			}

		}

		// Flush out the remaining indices
		for (int32 index : indices)
		{
			result->add_index(index);
		}

		for (const Ry::Vector3 position : positions)
		{
			Ry::Vertex1P1UV1C vert(position.x, position.y, position.z, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f);
			result->add_vertex(&vert);
		}

		result->update();

		return result;
	}

}