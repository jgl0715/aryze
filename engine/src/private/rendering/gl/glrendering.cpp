#include "glrendering.h"
#include "GL/glew.h"

#include "glshader.h"
#include "glvertexarray.h"
#include "gltexture.h"
#include "glframebuffer.h"

#include "GLFW/glfw3.h"

namespace Ry
{

	void GLRenderer::set_clear_color(const Vector4& color)
	{
		glClearColor(color.r, color.g, color.b, color.a);
	}
	
	void GLRenderer::clear_screen()
	{
		glClear(GL_COLOR_BUFFER_BIT);
	}

	void GLRenderer::set_render_mode(const RenderMode& mode)
	{
		switch (mode)
		{
		case Ry::RenderMode::FILLED:
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			break;
		case Ry::RenderMode::WIREFRAME:
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			break;
		}
	}

	void GLRenderer::set_viewport(int32 x, int32 y, int32 width, int32 height)
	{
		glViewport(x, y, width, height);
	}

	VertexArray* GLRenderer::make_vertex_array(const VertexFormat& format, bool with_index_buffer)
	{
		return new GLVertexArray(format, with_index_buffer);
	}
	
	Shader* GLRenderer::make_shader(const VertexFormat& format, const Ry::String& vertName, const Ry::String& fragName)
	{
		return new Ry::GLShader(format, vertName, fragName);
	}
	
	Texture* GLRenderer::make_texture()
	{
		return new GLTexture();
	}

	FrameBuffer* GLRenderer::make_framebuffer()
	{
		return new GLFrameBuffer();
	}

}