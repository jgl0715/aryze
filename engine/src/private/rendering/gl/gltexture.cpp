#include "gltexture.h"
#include <iostream>

namespace Ry
{

	GLTexture::GLTexture()
	{
		glGenTextures(1, &handle);

		std::cout << "OpenGL texture generated: " << handle << std::endl;

		// Setup texture parameters
		glBindTexture(GL_TEXTURE_2D, handle);
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

			//glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, float[])
		}
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	GLTexture::~GLTexture()
	{

	}

	void GLTexture::deleteTexture()
	{
		glDeleteTextures(1, &handle);
	}

	void GLTexture::bind()
	{
		glBindTexture(GL_TEXTURE_2D, handle);
	}

	void GLTexture::unbind()
	{
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	void GLTexture::data(uint32 width, uint32 height)
	{
		glBindTexture(GL_TEXTURE_2D, handle);
		{
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
		}
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	void GLTexture::data(uint8* data, PixelFormat format, uint32 width, uint32 height)
	{
		glBindTexture(GL_TEXTURE_2D, handle);
		{
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, to_gl_format(format), GL_UNSIGNED_BYTE, data);
		}
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	void GLTexture::data(float* data, PixelFormat format, uint32 width, uint32 height)
	{
		glBindTexture(GL_TEXTURE_2D, handle);
		{
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, to_gl_format(format), GL_FLOAT, data);
		}
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	void GLTexture::data_rgb(float* data, uint32 width, uint32 height)
	{
		glBindTexture(GL_TEXTURE_2D, handle);
		{
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGB, GL_FLOAT, data);
		}
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	GLuint GLTexture::get_handle()
	{
		return handle;
	}

	int32 GLTexture::to_gl_format(PixelFormat format)
	{
		int32 gl_format = GL_RGB;

		switch (format)
		{
		case PixelFormat::RGB:
			gl_format = GL_RGB;
			break;
		case PixelFormat::RGBA:
			gl_format = GL_RGBA;
			break;
		case PixelFormat::RED:
			gl_format = GL_RED;
			break;
		default:
			std::cout << "NON SUPPORTED FORMAT RETURNING GL_RGB " << (int32) format << std::endl;
		}

		return gl_format;
	}
}