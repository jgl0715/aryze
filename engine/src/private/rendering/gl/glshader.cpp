#pragma once

#include "glshader.h"
#include "util.h"
#include <iostream>
#include "file/file.h"

namespace Ry
{

	GLShader::GLShader(const VertexFormat& format, const String& vertName, const String& fragName):
		Shader(format, vertName, fragName)
	{
		this->vertName = vertName;
		this->fragName = fragName;

		// Create the entire OpenGL program.
		this->programHandle = glCreateProgram();

		// Create and compile the vertex and fragment shaders.
		int32 vertHandle = createShader(GL_VERTEX_SHADER, VERT_DIR, vertName);
		int32 fragHandle = createShader(GL_FRAGMENT_SHADER, FRAG_DIR, fragName);

		// Attach the compiled shaders.
		glAttachShader(programHandle, vertHandle);
		glAttachShader(programHandle, fragHandle);

		for (int32 i = 0; i < format.attribute_count; i++)
		{
			VertexAttrib attrib = format.attributes[i];
			String name = attrib.name;
			glBindAttribLocation(programHandle, i, *name);
		}

		// Link and check the status.
		glLinkProgram(programHandle);
		programStatusCheck(GL_LINK_STATUS, "link");

		// Validate and check the status.
		glValidateProgram(programHandle);
		programStatusCheck(GL_VALIDATE_STATUS, "validate");

		// Detach and delete unneeded resource.
		glDetachShader(programHandle, vertHandle);
		glDetachShader(programHandle, fragHandle);
		glDeleteShader(vertHandle);
		glDeleteShader(fragHandle);
	}

	void GLShader::deleteShader()
	{
		glDeleteProgram(programHandle);
	}

	void GLShader::uniformMat44(const Ry::String name, const float* data)
	{
		glProgramUniformMatrix4fv(programHandle, get_uniform_location(name), 1, true, data);
	}

	void GLShader::uniformMat44(const Ry::String name, const Matrix4& mat)
	{
		glProgramUniformMatrix4fv(programHandle, get_uniform_location(name), 1, true, *mat);
	}

	void GLShader::uniform_float(const Ry::String name, float v)
	{
		glProgramUniform1f(programHandle, get_uniform_location(name), v);
	}

	void GLShader::uniform_vec3(const Ry::String name, Vector3 vec)
	{
		glProgramUniform3f(programHandle, get_uniform_location(name), vec.x, vec.y, vec.z);
	}

	void GLShader::bind() const
	{
		glUseProgram(programHandle);
	}

	void GLShader::bind_vao(const VertexArray* vao) const
	{
	}

	void GLShader::unbind()
	{
		glUseProgram(0);
	}

	int32 GLShader::createShader(int32 type, const String& path, const String& name)
	{
		int32 handle = glCreateShader(type);
		String source = File::load_file_as_string(path + "/" + name);

		const char* data = source.getData();
		const char* const* ptr_to_data = &data;
		int32 size = source.getSize();
		glShaderSource(handle, 1, ptr_to_data, &size);

		glCompileShader(handle);

		int32 result;
		glGetShaderiv(handle, GL_COMPILE_STATUS, &result);

		if (result == GL_FALSE)
		{
			GLsizei length;
			GLchar info_log[1024];
			glGetShaderInfoLog(handle, 1024, &length, info_log);

			std::cerr << "Shader failed to compile: " << *name << std::endl;
			std::cerr << info_log << std::endl;
		}
		else
		{
			std::cout << *name << " compiled" << std::endl;
		}

		return handle;
	}

	GLint GLShader::get_uniform_location(Ry::String name)
	{
		if (uniforms.contains(name))
			return *uniforms.get(name);
		else
		{
			int32 location = glGetUniformLocation(programHandle, *name);
			uniforms.insert(name, location);
			return location;
		}
	}

	void GLShader::programStatusCheck(int32 type, const String& name)
	{
		int32 result;
		glGetProgramiv(programHandle, type, &result);

		if (result == GL_FALSE)
		{
			char log[2048];
			int length;
			glGetProgramInfoLog(programHandle, 2048, &length, log);

			std::cerr << "GLSL program " << *name << " error: " << std::endl;
			std::cerr << log << std::endl;
		}
	}
}