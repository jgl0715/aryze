#pragma once

#include "rendering/interface/framebuffer.h"
#include "core/core.h"
#include "gl/glew.h"

#define MAX_COLORS 3

namespace Ry
{
	class GLTexture;

	class GLFrameBuffer : public FrameBuffer
	{

	public:

		GLFrameBuffer();
		~GLFrameBuffer();

		/************************************************************************/
		/* Interface functions                                                  */
		/************************************************************************/
		virtual void bind();
		virtual bool check_status();
		virtual void unbind();
		virtual void attach_color_texture(Texture* texture, int32 index);
		virtual void setup();

	private:
		uint32 fbo_handle;
		GLenum index_to_gl_color(int32 index);

		int32 color_buffers;
		GLenum color_attachments[MAX_COLORS];

	};
}
