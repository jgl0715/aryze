#pragma once

#include "rendering/interface/shader.h"
#include "core/core.h"
#include "core/string.h"
#include "math/math.h"
#include "glvertexarray.h"
#include "core/map.h"

#define MAX_UNIFORMS 30

namespace Ry
{

	class ENGINE_API GLShader : public Shader
	{
	public:

		GLShader(const VertexFormat& format, const String& vertName, const String& fragName);

		/************************************************************************/
		/* Interface functions                                                  */
		/************************************************************************/

		virtual void deleteShader();
		virtual void uniformMat44(const Ry::String name, const float* data);
		virtual void uniformMat44(const Ry::String name, const Matrix4& mat);
		virtual void uniform_float(const Ry::String name, float v);
		virtual void uniform_vec3(const Ry::String name, Vector3 vec);
		virtual void bind_vao(const VertexArray* vao) const;
		virtual void bind() const;
		virtual void unbind();

	private:

		String vertName;
		String fragName;
		uint32 programHandle;

		Map<Ry::String, int32> uniforms;

		int32 createShader(int32 type, const String& path, const String& name);
		GLint get_uniform_location(Ry::String name);
		void programStatusCheck(int32 type, const String& name);
	};

}