#pragma once

#include "rendering/interface/texture.h"
#include "core/core.h"
#include "GL/glew.h"

namespace Ry
{
	class ENGINE_API GLTexture : public Texture
	{
	public:

		GLTexture();
		~GLTexture();

		virtual void deleteTexture();
		virtual void bind();
		virtual void unbind();
		virtual void data(uint32 width, uint32 height);
		virtual void data(float* data, PixelFormat format, uint32 width, uint32 height);
		virtual void data(uint8* data, PixelFormat format, uint32 width, uint32 height);
		virtual void data_rgb(float* data, uint32 width, uint32 height);

		GLuint get_handle();

	private:

		int32 to_gl_format(PixelFormat format);

		GLuint handle;
	};
}
