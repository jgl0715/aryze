#include "glframebuffer.h"
#include "gltexture.h"
#include <iostream>

namespace Ry
{
	GLFrameBuffer::GLFrameBuffer()
	{
		glGenFramebuffers(1, &fbo_handle);

		this->color_buffers = 0;
	}

	GLFrameBuffer::~GLFrameBuffer()
	{

	}

	void GLFrameBuffer::bind()
	{
		glBindFramebuffer(GL_FRAMEBUFFER, fbo_handle);
	}

	void GLFrameBuffer::attach_color_texture(Texture* texture, int32 index)
	{
		GLTexture* gl_texture = static_cast<GLTexture*>(texture);

		if (gl_texture)
		{
			bind();

			int32 attachment = index_to_gl_color(index);

			glFramebufferTexture(GL_FRAMEBUFFER, attachment, gl_texture->get_handle(), 0);

			color_attachments[color_buffers] = attachment;
			color_buffers++;
		}
		else
		{
			std::cerr << "Only GL textures can be used for GL frame buffers." << std::endl;
		}
	}

	void GLFrameBuffer::setup()
	{
		glDrawBuffers(color_buffers, color_attachments);
	}

	bool GLFrameBuffer::check_status()
	{
		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			return false;
		}

		return true;
	}

	GLenum GLFrameBuffer::index_to_gl_color(int32 index)
	{
		switch (index)
		{
		case 0:
			return GL_COLOR_ATTACHMENT0;
		case 1:
			return GL_COLOR_ATTACHMENT1;
		case 2:
			return GL_COLOR_ATTACHMENT2;
		}

		return -1;
	}

	void GLFrameBuffer::unbind()
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

}