#pragma once

#include "rendering/rendering.h"

namespace Ry
{

	/**
	 * OpenGL implementation of rendering interface.
	 */
	class GLRenderer : public Renderer
	{

	public:

		virtual void set_clear_color(const Vector4& color);
		virtual void clear_screen();
		virtual void set_render_mode(const RenderMode& mode);
		virtual void set_viewport(int32 x, int32 y, int32 width, int32 height);

		virtual VertexArray* make_vertex_array(const VertexFormat& format, bool with_index_buffer);
		virtual Shader* make_shader(const VertexFormat& format, const Ry::String& vertName, const Ry::String& fragName);
		virtual Texture* make_texture();
		virtual FrameBuffer* make_framebuffer();

	};

}