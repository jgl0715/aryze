#include "GL/glew.h"
#include "GLFW/glfw3.h"
#include "desktop_app.h"
#include "core/core.h"
#include "file/file.h"
#include "globals.h"
#include <chrono>
#include <thread>
#include <cmath>
#include <iostream>
#include "util.h"
#include "rendering/gl/glrendering.h"

void error_callback(int error, const char* desc)
{
	std::cerr << "GLFW error: " << error << ": " << desc << std::endl;
}

void button_callback(GLFWwindow* window, int button, int action, int mods)
{
	if (action == GLFW_PRESS)
	{
		Ry::input_handler->setButton(button, true);
	}
	else if (action == GLFW_RELEASE)
	{
		Ry::input_handler->setButton(button, false);
	}
}

void scroll_wheel_callback(GLFWwindow* window, double x_off, double y_off)
{
	Ry::input_handler->setScroll(y_off);
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (action == GLFW_PRESS)
	{
		Ry::input_handler->setKey(key, true);
	}
	else if (action == GLFW_RELEASE)
	{
		Ry::input_handler->setKey(key, false);
	}
}

void char_callback(GLFWwindow* window, unsigned int codepoint)
{
	Ry::input_handler->sendChar(codepoint);
}

void window_size_callback(GLFWwindow* window, int width, int height)
{
	Ry::DesktopApp* app = static_cast<Ry::DesktopApp*>(Ry::app);

	if (!app->is_fullscreen())
	{

		if (app)
		{
			for (Ry::ScreenSizeListener* listener : app->size_listeners)
			{
				listener->resized(width, height);
			}
		}
	}
}

namespace Ry
{

	DesktopConfig::DesktopConfig()
	{
		window.title = "game";
		window.width = 800;
		window.height = 600;
		window.fullscreen = false;
		window.monitor = 0;
		busy = false;
		fps_lock = 60;
		game = nullptr;
	}

	DesktopConfig::DesktopConfig(const DesktopConfig& config)
	{
		busy = config.busy;
		fps_lock = config.fps_lock;
		window.title = config.window.title;
		window.width = config.window.width;
		window.height = config.window.height;
		window.fullscreen = config.window.fullscreen;
		window.monitor = config.window.monitor;
	}

	DesktopConfig& DesktopConfig::operator=(const DesktopConfig& config)
	{
		busy = config.busy;
		fps_lock = config.fps_lock;
		window.title = config.window.title;
		window.width = config.window.width;
		window.height = config.window.height;
		window.fullscreen = config.window.fullscreen;
		window.monitor = config.window.monitor;

		return *this;
	}

	DesktopApp::DesktopApp(AbstractGame* game, Ry::String app_name):
		Ry::Application(game, app_name)
	{
		this->fps = 0;
		this->frames = 0;
		this->running = false;

		app = this;
		input_handler = new InputHandler;
		
		// Load desktop application configuration
		load_config(app_name);
		config.game = game;
	}

	DesktopApp::~DesktopApp()
	{
		delete input_handler;
	}

	void DesktopApp::run()
	{
		init_engine();

		if (initialized)
		{
			std::cout << "Engine successfully initialized" << std::endl;

			running = true;


			// init subsystems here

			std::chrono::high_resolution_clock::time_point last_frame = std::chrono::high_resolution_clock::now();
			std::chrono::high_resolution_clock::time_point last_second = last_frame;
			uint64 frame_period = (uint64)std::floor(1e9 / this->config.fps_lock);

			while (running)
			{
				// Synchronize framerate 
				std::chrono::high_resolution_clock::time_point now = std::chrono::high_resolution_clock::now();
				std::chrono::nanoseconds delta_frame = now - last_frame;
				std::chrono::nanoseconds delta_second = now - last_second;

				// Amount of nanoseconds.
				uint64 frame_time_ns = delta_frame.count();
				uint64 second_time_ns = delta_second.count();

				if (frame_time_ns >= frame_period)
				{
					last_frame = now;

					float delta = (float)(frame_time_ns / 1e9);

					update(delta);
					render();

					frames++;
				}

				if (second_time_ns >= 1e9)
				{
					fps = frames;
					frames = 0;
					last_second = now;
//					std::cout << "FPS: " << fps << std::endl;
				}

				// Sleep the thread if not busy
				if (!config.busy)
				{
					std::this_thread::sleep_for(std::chrono::milliseconds(2));
				}

			}
		}
		else
		{
			std::cerr << "Engine failed to initialize" << std::endl;
		}
	}

	void DesktopApp::stop()
	{
		running = false;
	}

	int32 DesktopApp::get_width() const
	{
		if (is_fullscreen())
		{
			return glfwGetVideoMode(get_monitor_by_index(fullscreen_monitor))->width;
		}
		else
		{
			int32 width, height;
			glfwGetWindowSize(window, &width, &height);

			return width;
		}
	}


	int32 DesktopApp::get_height() const
	{
		if (is_fullscreen())
		{
			return glfwGetVideoMode(get_monitor_by_index(fullscreen_monitor))->height;
		}
		else
		{
			int32 width, height;
			glfwGetWindowSize(window, &width, &height);

			return height;
		}
	}

	int32 DesktopApp::get_fps() const
	{
		return fps;
	}

	bool DesktopApp::is_fullscreen() const
	{
		return glfwGetWindowMonitor(window) != nullptr;
	}


	void DesktopApp::set_fullscreen(bool fullscreen)
	{
		this->set_fullscreen(fullscreen, 0);
		this->set_fullscreen(fullscreen, 0);
	}

	void DesktopApp::set_fullscreen(bool fullscreen, int32 monitor)
	{
		if (is_fullscreen() == fullscreen)
			return;

		if (fullscreen)
		{
			// backup window position and window size

			glfwGetWindowPos(window, &window_pos[0], &window_pos[1]);
			glfwGetWindowSize(window, &window_size[0], &window_size[1]);

			// get resolution of monitor
			GLFWmonitor* mon = get_monitor_by_index(monitor);
			const GLFWvidmode* mode = glfwGetVideoMode(mon);

			window_size_callback(window, mode->width, mode->height);

			// switch to full screen
			glfwSetWindowMonitor(window, mon, 0, 0, mode->width, mode->height, 0);

			fullscreen_monitor = monitor;
		}
		else
		{
			// restore last window size and position
			glfwSetWindowMonitor(window, nullptr, window_pos[0], window_pos[1], window_size[0], window_size[1], 0);
			
			fullscreen_monitor = -1;
		}

	}

	GLFWmonitor* DesktopApp::get_monitor_by_index(int32 index) const
	{
		// Get the correct monitor
		int count;
		GLFWmonitor** monitors = glfwGetMonitors(&count);
		GLFWmonitor* monitor = nullptr;

		if (index >= count)
		{
			std::cerr << "Full screen monitor " << index << " out of bounds." << std::endl;
			return monitors[0];
		}
		else
		{
			return monitors[index];
		}
	}

	void DesktopApp::init_engine()
	{
		initialized = true;

		init_window();

		init_resources();

		if (initialized)
		{
			if (config.game)
			{
				std::cout << "Initializing game" << std::endl;
				config.game->init();

				// Send initial window resize event
				window_size_callback(window, get_width(), get_height());
			}
			else
			{
				std::cerr << "Game was nullptr" << std::endl;
				initialized = false;
			}

		}
	}

	void DesktopApp::init_resources()
	{
#ifdef COMPILE_DLL
	#ifdef _WIN32
		system("xcopy /y /E ..\\..\\..\\engine\\res .\\");
	#endif
#endif
	}

	void DesktopApp::init_window()
	{

		if (!glfwInit())
		{
			std::cerr << "GLFW initialization failed!" << std::endl;

			initialized = false;
		}
		else
		{
			std::cout << "GLFW initialized" << std::endl;

			glfwSetErrorCallback(&error_callback);

			// Specify windowing hints
			switch (Ry::rplatform)
			{
			case RenderingPlatform::GL4:
				//glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
				//glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

				// For MSAA
				//glfwWindowHint(GLFW_SAMPLES, 4);

				break;
			}

			if (config.window.fullscreen)
			{
				GLFWmonitor* monitor = get_monitor_by_index(config.window.monitor);

				if (monitor)
				{
					const GLFWvidmode* mode = glfwGetVideoMode(monitor);
					window = glfwCreateWindow(mode->width, mode->height, config.window.title.getData(), monitor, NULL);
					fullscreen_monitor = config.window.monitor;
				}
				else
				{
					window = glfwCreateWindow(config.window.width, config.window.height, config.window.title.getData(), NULL, NULL);
					fullscreen_monitor = -1;
				}
			}
			else
			{
				window = glfwCreateWindow(config.window.width, config.window.height, config.window.title.getData(), NULL, NULL);
				fullscreen_monitor = -1;
			}


			if (!window)
			{
				std::cerr << "GLFW window creation failed" << std::endl;
				initialized = false;
			}
			else
			{
				std::cout << "Window created" << std::endl;

				// Setup rendering target platform
				switch (Ry::rplatform)
				{
				case RenderingPlatform::GL4:

					rapi = new GLRenderer;

					glfwMakeContextCurrent(window);

					if (glewInit() != GLEW_OK)
					{
						initialized = false;
						std::cerr << "Failed to load OpenGL symbols" << std::endl;
					}
					else
					{
						std::cout << "OpenGL symbols loaded" << std::endl;

						// Setup OpenGL state
						glEnable(GL_TEXTURE_2D);
						glEnable(GL_DEPTH_TEST);
						glDisable(GL_BLEND);
						glDisable(GL_MULTISAMPLE);

						int major;
						int minor;
						glGetIntegerv(GL_MAJOR_VERSION, &major);
						glGetIntegerv(GL_MINOR_VERSION, &minor);

						std::cout << "GL version: " << major << "." << minor << std::endl;
					}

					break;
				}


				// Setup input callbacks
				glfwSetKeyCallback(window, &key_callback);
				glfwSetCharCallback(window, &char_callback);
				glfwSetMouseButtonCallback(window, &button_callback);
				glfwSetScrollCallback(window, &scroll_wheel_callback);

				// Setup window callbacks
				glfwSetWindowSizeCallback(window, &window_size_callback);
			}

		}
	}

	void DesktopApp::init_renderer()
	{

	}

	void DesktopApp::save_config(const Ry::String& name)
	{
		Ry::String file;

		file += "busy=" + Ry::to_string(config.busy) + "\n";
		file += "fps_lock=" + Ry::to_string(config.fps_lock) + "\n";
		file += "window.title=" + config.window.title + "\n";
		file += "window.width=" + Ry::to_string(config.window.width) + "\n";
		file += "window.height=" + Ry::to_string(config.window.height) + "\n";
		file += "window.fullscreen=" + Ry::to_string(config.window.fullscreen) + "\n";
		file += "window.monitor=" + Ry::to_string(config.window.monitor) + "\n";

		Ry::File::write_file(name + ".ini", file);
	}

	void DesktopApp::load_config(const Ry::String& name)
	{
		// Create a default config
		config.busy = false;
		config.fps_lock = 60;
		config.game = nullptr;
		config.window.title = "My Game";
		config.window.width = 800;
		config.window.height = 600;
		config.window.fullscreen = false;
		config.window.monitor = 0;

		if (Ry::File::does_file_exist(name + ".ini"))
		{
			Ry::String file = Ry::File::load_file_as_string(name + ".ini");
			Ry::String* items;
			int32 item_count = file.split("\n", &items);

			for (int32 i = 0; i < item_count; i++)
			{
				Ry::String& config_item = items[i];

				Ry::String* tokens;
				int32 token_count = config_item.split("=", &tokens);

				Ry::String& key = tokens[0];
				Ry::String& value = tokens[1];

				if (key == "busy")
				{
					config.busy = value.to_bool();
				}
				else if (key == "fps_lock")
				{
					config.fps_lock = value.to_int32();
				}
				else if (key == "window.title")
				{
					config.window.title = value;
				}
				else if (key == "window.width")
				{
					config.window.width = value.to_int32();
				}
				else if (key == "window.height")
				{
					config.window.height = value.to_int32();
				}
				else if (key == "window.fullscreen")
				{
					config.window.fullscreen = value.to_bool();
				}
				else if (key == "window.monitor")
				{
					config.window.monitor = value.to_int32();
				}

			}
		}
		else
		{
			save_config(name);
		}
	}

	void DesktopApp::update(float delta)
	{
		if (glfwWindowShouldClose(window))
		{
			stop();
		}

		glfwPollEvents();

		double mouse_x, mouse_y;
		glfwGetCursorPos(window, &mouse_x, &mouse_y);

		input_handler->setMouse((float)mouse_x, (float)mouse_y);

		config.game->update(delta);
	}

	void DesktopApp::render()
	{

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glClearColor(0.2f, 0.2f, 0.2f, 1.0f);

		config.game->render();

		glfwSwapBuffers(window);
	}

	void DesktopApp::exit()
	{
		std::cout << "Shutting down game" << std::endl;
		config.game->quit();

		// Delete the game
		delete config.game;

		std::cout << "Shutting down window" << std::endl;
		glfwDestroyWindow(window);
		glfwTerminate();
	}

}