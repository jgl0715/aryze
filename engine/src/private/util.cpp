#include "util.h"

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "stb/stb_image.h"
#include "globals.h"

namespace Ry
{

	Texture* load_texture(const String& path)
	{
		int32 width, height, channels;
		uint8* data = stbi_load(*path, &width, &height, &channels, 0);

		if (!data)
		{
			std::cerr << "Failed to load image: " << *path << std::endl;
			return nullptr;
		}
		else
		{
			Texture* texture = rapi->make_texture();

			if (channels == 3)
			{
				texture->data(data, PixelFormat::RGB, width, height);
			}
			else if (channels = 4)
			{
				texture->data(data, PixelFormat::RGBA, width, height);
			}

			return texture;
		}

	}

	Texture* generate_red_texture(uint32 width, uint32 height)
	{
		Ry::Texture* texture = rapi->make_texture();
		float* data = new float[(uint64)(width)*uint64(height) * 3L];

		for (uint32 i = 0; i < width * height; i++)
		{
			data[i * 3 + 0] = 1.0f;
			data[i * 3 + 1] = 0.0f;
			data[i * 3 + 2] = 0.0f;
		}

		texture->data(data, PixelFormat::RGB, width, height);

		return texture;
	}


}