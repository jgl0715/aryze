#include "globals.h"
#include "desktop_app.h"

namespace Ry
{
	
	Application* app = nullptr;
	InputHandler* input_handler = nullptr;

	Renderer* rapi = nullptr;
	RenderingPlatform rplatform = RenderingPlatform::GL4;

	Ry::Application* make_application(const Ry::String& app_name, Ry::AbstractGame* game, const Ry::Platform& platform)
	{
		Ry::Application* app = nullptr;
		switch (platform)
		{
		case Platform::WINDOWS:
			app = new Ry::DesktopApp(game, app_name);
		}

		return app;
	}

}