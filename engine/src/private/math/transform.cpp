#include "math/transform.h"


namespace Ry
{

	Vector3 Transform::get_forward() const
	{
		Vector4 result = get_rotation_mat() * Vector4(0.0f, 0.0f, -1.0f, 1.0f);

		return normalized(Vector3(result.x, result.y, result.z));
	}

	Vector3 Transform::get_right() const
	{
		Vector4 result = get_rotation_mat() * Vector4(-1.0f, 0.0f, 0.0f, 1.0f);

		return normalized(Vector3(result.x, result.y, result.z));
	}

	Vector3 Transform::get_up() const
	{
		Vector4 result = get_rotation_mat() * Vector4(0.0f, 1.0f, 0.0f, 1.0f);

		return normalized(Vector3(result.x, result.y, result.z));
	}

	Matrix4 Transform::get_rotation_mat() const
	{
		Matrix4 rotation_mat_x = rotatex4(rotation.x);
		Matrix4 rotation_mat_y = rotatey4(rotation.y);
		Matrix4 rotation_mat_z = rotatez4(rotation.z);

		return rotation_mat_z * rotation_mat_y * rotation_mat_x;
	}

	Matrix4 Transform::get_transform() const
	{
		Matrix4 translation_mat = translation4(position.x, position.y, position.z);
		Matrix4 rotation_mat_x = rotatex4(rotation.x);
		Matrix4 rotation_mat_y = rotatey4(rotation.y);
		Matrix4 rotation_mat_z = rotatez4(rotation.z);
		Matrix4 scale_mat = scale4(scale.x, scale.y, scale.z);

		return translation_mat * rotation_mat_z * rotation_mat_y * rotation_mat_x * scale_mat;
	}

	Matrix4 Transform::get_inverse_transform() const
	{
		Matrix4 translation_mat = translation4(position.x, position.y, position.z);
		Matrix4 scale_mat = scale4(scale.x, scale.y, scale.z);

		return inverse(translation_mat * rotatez4(rotation.z) * rotatey4(rotation.y) * rotatex4(rotation.x) * scale_mat);
	}

	Transform linear_interp(const Transform& a, const Transform& b, float delta)
	{
		Transform res;
		return res;
	}

}