#include "file/file.h"
#include <fstream>
#include <sstream>
#include <iostream>

namespace Ry
{
	namespace File
	{

		String load_file_as_string(const String& path)
		{
			std::ostringstream output_string;
			std::ifstream file_input(path.getData());
			std::string line;

			if (!file_input.is_open())
			{
				std::cerr << "Failed to read file " << path.getData() << std::endl;
			}
			else
			{
				while (std::getline(file_input, line))
				{
					output_string << line << "\n";
				}

				return String(output_string.str().c_str());
			}

			return String("");
		}

		void write_file(const String& path, const String& contents)
		{
			std::ofstream os(*path);
			os << *contents;
			os.close();
		}

		bool does_file_exist(const String& path)
		{
			return static_cast<bool>(std::ifstream(*path));
		}

	}
}