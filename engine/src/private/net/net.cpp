#include "net/net.h"
#include "net/websocketpp.h"

namespace Ry
{
	namespace Net
	{
		WebSocket* make_websock_server(int16 port)
		{
			return new WebSocketPP(port);
		}
	}
}