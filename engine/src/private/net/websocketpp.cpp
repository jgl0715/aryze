#include "net/websocketpp.h"
#include "websocketpp/endpoint.hpp"

namespace Ry
{
	WebSocketPP::WebSocketPP(int16 port):
		WebSocket(port)
	{
//		server.set_error_channels(websocketpp::log::elevel::all);
// 		server.set_access_channels(websocketpp::log::alevel::all ^ websocketpp::log::alevel::frame_payload);

		server.clear_access_channels(websocketpp::log::alevel::all);

		server.init_asio();
		server.listen(port);
	}

	WebSocketPP::~WebSocketPP()
	{

	}

	void WebSocketPP::start()
	{
		// Message handler
		server.set_message_handler(
			[this](websocketpp::connection_hdl hdl, Server::message_ptr msg_ptr)->void
			{
				// Convert payload to engine string.
				const char* data = msg_ptr->get_payload().c_str();
				Ry::String string(data);

				// Invoke all registered message handlers.
				for (const MessageHandler handler : message_handlers)
				{
					Ry::WebSocketPPConnection connection;
					connection.con = hdl;

					handler(&connection, data);
				}
			}
		);

		// Connection handler
		server.set_open_handler(
			[this](websocketpp::connection_hdl hdl)->void
			{
				Ry::WebSocketPPConnection* connection = new Ry::WebSocketPPConnection();
				Ry::WebSocketPP* socket_pp = static_cast<WebSocketPP*>(this);
				connection->con = hdl;

				socket_pp->connections.insert(connection);
				
				// Invoke all registered connection handlers.
				for (const ConnectionHandler handler : connection_handlers)
				{
					handler(connection);
				}

			}
		);


		// Start the server
		server.start_accept();
		server.run();
	}

	void WebSocketPP::send_message(Ry::WebSocketConnection* connection, const Ry::String& message)
	{
		const WebSocketPPConnection* connection_pp = static_cast<const WebSocketPPConnection*>(connection);

		if (connection_pp)
		{
			server.send(connection_pp->con, std::string(*message), websocketpp::frame::opcode::text);
		}
		else
		{
			std::cout << "Can only use websocket++ websockets with websocket++ connections" << std::endl;
		}
	}

}