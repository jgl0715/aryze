#include "../public/application.h"

namespace Ry
{

	Application::Application(AbstractGame* game, Ry::String app_name):
		running(false)
	{

	}

	Application::~Application()
	{

	}

	bool Application::is_running() const
	{
		return running;
	}

	Ry::String Application::get_app_name() const
	{
		return app_name;
	}

	void Application::add_screen_size_listener(ScreenSizeListener* listener)
	{
		size_listeners.emplace(listener);
	}
	
	void Application::remove_screen_size_listener(ScreenSizeListener* listener)
	{
		size_listeners.erase(listener);
	}
}