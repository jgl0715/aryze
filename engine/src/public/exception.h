#pragma once

#include <string>
#include "core/core.h"

namespace Ry
{

	/**
	 * The base exception class.
	 */
	struct ENGINE_API Exception
	{
		std::string excep_msg;
	};
}