#pragma once

#include "core/core.h"
#include "core/string.h"

namespace Ry
{
	class Texture;

	/**
	 * Loads an image resource as a texture.
	 * @param path The path to the image.
	 * @return Texture* A pointer to the resulting texture.
	 */
	ENGINE_API Texture* load_texture(const String& path);

	/**
	 * Generates a pure red texture.
	 * @param width The width of the texture.
	 * @param height The height of the texture.
	 * @return Texture* The resulting texture
	 */
	ENGINE_API Texture* generate_red_texture(uint32 width, uint32 height);

}