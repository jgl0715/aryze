#pragma once

#include "core/core.h"
#include <set>

#define MAX_KEYS 256
#define MAX_BUTTONS 256

namespace Ry
{

	struct GLFWwindow;

	/**
	 * A button listener that receives input for mouse buttons.
	 */
	class ENGINE_API ButtonListener
	{

	public:

		ButtonListener() {};
		virtual ~ButtonListener() {};

		virtual void onButtonPressed(int key) = 0;
		virtual void onButtonReleased(int key) = 0;

	};

	/**
	 * A listener for key events.
	 */
	class ENGINE_API KeyListener
	{

	public:

		KeyListener() {};
		virtual ~KeyListener() {};

		virtual void onKeyPressed(int key) = 0;
		virtual void onKeyReleased(int key) = 0;

	};

	/**
	 * A listener for character input events.
	 */
	class ENGINE_API CharListener
	{

	public:

		CharListener() {};
		virtual ~CharListener() {};

		virtual void onChar(unsigned int codepoint) = 0;
	};

	/**
	* A listener for scroll input events.
	*/
	class ENGINE_API ScrollListener
	{

	public:

		ScrollListener() {};
		virtual ~ScrollListener() {};

		virtual void onScroll(double amount) = 0;
	};

	/**
	 * Main class responsible for providing input information.
	 */
	class InputHandler
	{

	public:

		InputHandler();
		~InputHandler();

		/**
		 * @param key The key code to query.
		 * @return bool Whether the specified key code is pressed.
		 */
		ENGINE_API bool isKeyDown(int32 key);

		/**
		 * @param button The button code to query.
		 * @return bool Whether the specified button code is pressed.
		 */
		ENGINE_API bool isButtonDown(int32 button);
		
		/**
		 * @return float The pixel x position of the cursor.
		 */
		ENGINE_API float getX() const;
		
		/**
		 * @return float The pixel y position of the cursor.
		 */
		ENGINE_API float getY() const;

		/**
		 * @return float The differential x coordinate of the cursor.
		 */
		ENGINE_API float getDx() const;

		/**
		 * @return float The differential y coordinate of the cursor.
		 */
		ENGINE_API float getDy() const;

		/**
		* @return float The scroll amount.
		*/
		ENGINE_API float getScroll() const;

		/**
		 * Registers a new key listener to receive key input events.
		 * @param listener The key listener.
		 */
		ENGINE_API void addKeyListener(KeyListener* listener);

		/**
		 * Removes an already registered key listener.
		 * @param listener The key listener to remove.
		 */
		ENGINE_API void removeKeyListener(KeyListener* listener);

		/**
		 * Registers a new button listener to receive button input events.
		 * @param listener The button listener.
		 */
		ENGINE_API void addButtonListener(ButtonListener* listener);
		
		/**
		 * Removes an already registered button listener.
		 * @param listener The button listener to remove.
		 */
		ENGINE_API void removeButtonListener(ButtonListener* listener);

		/**
		 * Registers a new character listener to receive character input events.
		 * @param listener The character listener.
		 */
		ENGINE_API void addCharListener(CharListener* listener);

		/**
		 * Removes an already registered character listener.
		 * @param listener The character listener to remove.
		 */
		ENGINE_API void removeCharListener(CharListener* listener);

		/**
		 * Registers a new scroll listener to receive scroll input events.
		 * @param listener The scroll listener.
		 */
		ENGINE_API void addScrollListener(ScrollListener* listener);

		/**
		 * Removes an already registered scroll listener.
		 * @param listener The scroll listener to remove.
		 */
		ENGINE_API void removeScrollListener(ScrollListener* listener);

		// Functions not exposed in the API
		void setKey(int32 key, bool value);
		void setButton(int32 key, bool value);
		void setScroll(double scroll);
		void sendChar(unsigned int codepoint);
		void setMouse(float x, float y);

	private:

		std::set<KeyListener*> keyListeners;
		std::set<ButtonListener*> buttonListeners;
		std::set<CharListener*> charListeners;
		std::set<ScrollListener*> scrollListeners;

		bool keys_down[MAX_KEYS];
		bool buttons_down[MAX_BUTTONS];

		float mouse_x;
		float mouse_y;
		float last_mouse_x;
		float last_mouse_y;

		double scroll;
		double last_scroll;

	};

}