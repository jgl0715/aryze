#pragma once
#include "core.h"
#include <vector>
#include "GL/glew.h"
#include "math/math.h"

#define VERT_DIR "./shaders/vertex"
#define FRAG_DIR "./shaders/fragment"

namespace Ry
{

	struct ENGINE_API VertexAttrib
	{
		String name;
		int32 size;

		VertexAttrib()
		{
			name = "";
			size = 0;
		}

		VertexAttrib(const String& n, int32 si)
		{
			name = n;
			size = si;
		}

		bool operator==(const VertexAttrib& other);
	};

	extern ENGINE_API VertexAttrib pos;
	extern ENGINE_API VertexAttrib color;
	extern ENGINE_API VertexAttrib tex_coord;

	extern ENGINE_API VertexAttrib POS[1];
	extern ENGINE_API VertexAttrib POS_COLOR[2];
	extern ENGINE_API VertexAttrib POS_TEXCOORD[2];
	extern ENGINE_API VertexAttrib POS_TEXCOORD_COLOR[3];

	struct ENGINE_API VertexAttribPtr
	{
		VertexAttrib attrib;
		int32 offset;
	};

	class VertexArray
	{
	public:

		VertexArray(int32 maxAttribs);
		VertexArray(int32 maxAttribs, bool withIndexBuffer);
		~VertexArray();

		void push_vert_data(float* data, uint32 verts, uint32 element_size, int32 hint);
		void push_index_data(uint32* data, uint32 indices, int32 hint);

		void pushAttrib(const VertexAttrib& attrib);
		void pushAttrib(const String& name, int32 size);
		void popAttrib();

		void render(int32 primitive) const;

		int32 get_attrib_count() const;
		String get_attrib_name(int32 index) const;

		void deleteArray();

	private:

		GLuint vao;
		GLuint vbo;
		GLuint ibo;

		int32 attribCount;
		VertexAttribPtr* attribs;
		bool withIndexBuffer;

		int32 verts;
		int32 element_size;
		int32 indices;
	};

	class ENGINE_API Shader
	{
	public:
		Shader(const String& vertName, const String& fragName);

		void deleteShader();
		void uniformMat44(int32 location, const float* data);
		void uniformMat44(int32 location, const Matrix4& mat);
		void uniform_float(int32 location, float v);

		void bind_vao(const VertexArray* vao) const;
		void bind() const;
		static void bindNone();

	private:

		String vertName;
		String fragName;
		uint32 programHandle;

		int32 createShader(int32 type, const String& path, const String& name);

		void programStatusCheck(int32 type, const String& name);
	};

	class ENGINE_API Texture
	{
	public:

		Texture();

		~Texture();

		void deleteTexture();
		void bind();
		void data(uint32 width, uint32 height);
		void data(float* data, int32 format, uint32 width, uint32 height);
		void data_rgb(float* data, uint32 width, uint32 height);

		void data(uint8* data, int32 format, uint32 width, uint32 height);

		GLuint get_handle();

	private:

		GLuint handle;
	};
}