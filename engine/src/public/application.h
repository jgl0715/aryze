#pragma once

#include "core/core.h"
#include "core/string.h"
#include <set>

namespace Ry
{

	class ENGINE_API ScreenSizeListener
	{
	public:
		virtual void resized(int new_width, int new_height) = 0;
	};

	/**
	 * Abstract class that should be overridden by applications.
	 */
	class ENGINE_API AbstractGame
	{

	public:

		/**
		 * Initializes the application. All resources should be allocated in this function.
		 */
		virtual void init() = 0;

		/**
		 * Called each frame when the application should update. Only application logic should occur in this function. The frequency of this is defined in the application and is platform dependent.
		 */
		virtual void update(float delta) = 0;

		/**
		 * Called each frame when the application should render. The frequency of this is defined in the application and is platform dependent.
		 */
		virtual void render() = 0;

		/**
		 * The application should free its resources and perform necessary cleanup in this function.
		 */
		virtual void quit() = 0;

	};

	/**
	 * Base application class for all platforms.
	 */
	class Application
	{
	public:

		/**
		 * Creates a new application with the specified abstract game.
		 *
		 * @param game The game that will be used for the application.
		 */
		Application(AbstractGame* game, Ry::String app_name);

		/**
		 * Frees allocated resources.
		 */
		virtual ~Application();

		/**
		 * Starts the application synchronously. This function will exit whenever the application has been successfully terminated.
		 */
		virtual void run() = 0;

		/**
		 * Sends the command to synchronously terminate this application. The current frame will be the last frame.
		 */
		virtual void stop() = 0;

		/**
		 * @return int32 The width of the device screen in pixels.
		 */
		virtual int32 get_width() const = 0;

		/**
		 * @return int32 The height of the device screen in pixels.
		 */
		virtual int32 get_height() const = 0;

		/**
		 * @return int32 The current operating frames per second the application is achieving.
		 */
		virtual int32 get_fps() const = 0;

		/**
		 * @return bool True if the application is in a loop, false if not.
		 */
		bool is_running() const;

		Ry::String get_app_name() const;

		ENGINE_API void add_screen_size_listener(ScreenSizeListener* listener);
		ENGINE_API void remove_screen_size_listener(ScreenSizeListener* listener);

		std::set<Ry::ScreenSizeListener*> size_listeners;

	protected:
		bool running;

	private:

		Ry::String app_name;

	};
}