#include "net/websocket.h"
#include "websocketpp/server.hpp"
#include "websocketpp/config/asio_no_tls.hpp"
#include <set>

typedef websocketpp::server<websocketpp::config::asio> Server;

namespace Ry
{

	class WebSocketPPConnection : public WebSocketConnection
	{

	public:

		WebSocketPPConnection()
		{

		}

		WebSocketPPConnection(const WebSocketPPConnection& o)
		{
			con = o.con;
		}

		const WebSocketPPConnection& operator=(const WebSocketPPConnection& o)
		{
			con = o.con;
		}

		websocketpp::connection_hdl con;
	};

	class WebSocketPP : public WebSocket
	{
	public:

		ENGINE_API WebSocketPP(int16 port);
		ENGINE_API ~WebSocketPP();

		ENGINE_API virtual void start();

		ENGINE_API virtual void send_message(Ry::WebSocketConnection* connection, const Ry::String& message);

	public:

		Server server;
		std::set<WebSocketPPConnection*> connections;
	};

}