#pragma once

#include "core/core.h"
#include "net/websocket.h"

namespace Ry
{

	namespace Net
	{
		ENGINE_API WebSocket* make_websock_server(int16 port);
	}

}
