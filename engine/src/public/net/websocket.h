#pragma once

#include "core/core.h"
#include "core/string.h"

#include <vector>

namespace Ry
{

	class WebSocketConnection
	{
	public:
		ENGINE_API WebSocketConnection()
		{

		}

		ENGINE_API ~WebSocketConnection()
		{

		}
	};

	typedef void (*MessageHandler)(Ry::WebSocketConnection* connection, const Ry::String&);
	typedef void (*ConnectionHandler)(Ry::WebSocketConnection* connection);

	class WebSocket
	{

	public:

		ENGINE_API WebSocket(int16 port);
		ENGINE_API ~WebSocket();

		ENGINE_API void add_message_handler(MessageHandler handler);
		ENGINE_API void add_connection_handler(ConnectionHandler handler);

		ENGINE_API virtual void send_message(WebSocketConnection* conn, const Ry::String& message) = 0;
		ENGINE_API virtual void start() = 0;


	protected:

		std::vector<MessageHandler> message_handlers;
		std::vector<ConnectionHandler> connection_handlers;

	};

}