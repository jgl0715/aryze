#pragma once

#include "application.h"
#include "input.h"
#include "rendering/rendering.h"
#include "net/net.h"

namespace Ry
{

	enum class ENGINE_API Platform
	{
		WINDOWS, MAC, LINUX
	};

	/**
	 * The global application.
	 */
	extern ENGINE_API Application* app;

	/**
	 * The global input handler.
	 */
	extern ENGINE_API InputHandler* input_handler;

	/**
	 * The global rendering API.
	 */
	extern ENGINE_API Renderer* rapi;

	/**
	 * The rendering platform.
	 */
	extern ENGINE_API RenderingPlatform rplatform;

	ENGINE_API Ry::Application* make_application(const Ry::String& app_name, Ry::AbstractGame* game, const Ry::Platform& platform);

}