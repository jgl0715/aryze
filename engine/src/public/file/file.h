#pragma once

#include "core/core.h"
#include "core/string.h"

namespace Ry
{

	namespace File
	{

		/**
		 * Loads the contents of a text as as a string, where each line is delimited by the newline character.
		 * @param path The path to the file to load
		 * @return String The contents of the file as a String, where each line is separated by a newline.
		 */
		ENGINE_API String load_file_as_string(const String& path);

		/**
		 * Writes a string to a file.
		 * @param path The path to the file to write to.
		 * @param contents The contents to write out to the file.
		 */
		ENGINE_API void write_file(const String& path, const String& contents);


		/**
		 * Checks whether a system file exists.
		 * @param path The path to the system file.
		 * @return bool Whether the file exists.
		 */
		ENGINE_API bool does_file_exist(const String& path);

	}

}
