#pragma once

#include "core/core.h"

#define TABLE_SIZE 500

namespace Ry
{
	template <class K, class V>
	struct ENGINE_API MapChain
	{
		K key;
		V value;
		MapChain<K, V>* next;
	};

	template <class K, class V>
	class ENGINE_API Map
	{
	public:

		Map()
		{
			for (uint32 i = 0; i < TABLE_SIZE; i++)
				table[i] = nullptr;
		}

		bool contains(const K& key)
		{
			uint32 hash_value = key();
			uint32 bucket = hash_value % TABLE_SIZE;
			MapChain<K, V>* chain = table[bucket];

			if (chain == nullptr)
				return false;

			while (chain != nullptr && chain->key != key)
				chain = chain->next;

			if (chain == nullptr)
				return false;
			else
				return true;
		}

		void insert(const K& key, const V& value)
		{
			uint32 hash_value = key();
			uint32 bucket = hash_value % TABLE_SIZE;

			MapChain<K, V>* new_chain = new MapChain<K, V>();
			new_chain->key = key;
			new_chain->value = value;
			new_chain->next = nullptr;

			// Travel to the last element in the bucket
			MapChain<K, V>* head = table[bucket];
			while (head != nullptr && head->next != nullptr)
				head = head->next;

			if (head == nullptr)
			{
				table[bucket] = new_chain;
			}
			else
			{
				head->next = new_chain;
			}

		}

		V* get(const K& key)
		{
			uint32 hash_value = key();
			MapChain<K, V>* bucket = table[hash_value % TABLE_SIZE];

			if (bucket == nullptr)
				return nullptr;

			// Go through elements in this bucket until we find a match, or we reach the end.
			while (bucket != nullptr && bucket->key != key)
			{
				bucket = bucket->next;
			}

			if (bucket == nullptr)
				return nullptr;

			return &bucket->value;
		}

		void remove(const K& key)
		{
			uint32 hash_value = K->operator();
			uint32 bucket = hash_value % TABLE_SIZE;
			MapChain<K, V>* chain = table[bucket];

			if (chain == nullptr)
			{
				std::cerr << "ERROR: tried to delete element that was not in hashmap" << std::endl;
			}
			else if (chain->next == nullptr)
			{
				if (chain->key == key)
					table[bucket] = nullptr;
				else
					std::cerr << "ERROR: tried to delete element that was not in hashmap" << std::endl;
			}
			else
			{
				MapChain<K, V>* prev_chain = table[bucket];
				chain = chain->next;

				while (chain != nullptr && chain->key != key)
				{
					chain = chain->next;
					bucket = bucket->next;
				}

				if (chain != nullptr)
				{
					prev_chain->next = nullptr;
					delete chain;
				}
				else
				{
					std::cerr << "ERROR: tried to delete element that was not in hashmap" << std::endl;
				}
			}
		}

	private:

		MapChain<K, V>* table[TABLE_SIZE];
	};

}