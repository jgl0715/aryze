#pragma once

#include "core.h"

namespace Ry
{

	/**
	 * Engine implementation of string. Immutable and null terminated.
	 */
	class ENGINE_API String
	{

	public:

		String();
		String(const char* dat);
		String(const String& other);
		~String();

		/**
		 * Returns raw c string representation.
		 */
		const char* getData() const;

		/**
		 * The amount of characters contained in this string, not including the null terminator.
		 */
		uint32 getSize() const;

		/**
		* @return String The same string, will all charcters in lower case.
		*/
		String to_lower() const;

		void operator=(const String& other);
		bool operator==(const String& other) const;
		bool operator==(const char* other) const;
		bool operator!=(const String& other) const;
		bool operator!=(const char* other) const;
		uint32 operator()() const;

		/**
		 * Standard string concatenation.
		 */
		String operator+(const String& other) const;
		
		/**
		 * Standard string concatenation with a c string.
		 */
		String operator+(const char* other) const;
		
		/**
		 * 
		 */
		String& operator+=(const String& other);

		/**
		 * Converts this string to a double.
		 * @return double The double representation.
		 */
		double to_double() const;
		
		/**
		 * Converts this string to a float.
		 * @return float The float representation.
		 */
		float to_float() const;

		/**
		 * Converts this string to a 32 bit signed int.
		 * @return int32 The int32 representation.
		 */
		int32 to_int32() const;
		
		/**
		* Converts this string to a 64 bit unsigned int.
		* @return uint32 The uint32 representation.
		*/
		uint32 to_uint32() const;

		/**
		* Converts this string to a boolean.
		* The following are parsed as true: True, TRUE, true, T, 1
		* Everything else is interpreted as false.
		* @return bool The boolean representation.
		*/
		bool to_bool() const;

		/**
		 * Overloading for adding a string to a c string and returning an engine string.
		 * @param char* A c string
		 * @param String An engine string
		 * @return String The resulting engine string
		 */
		friend String operator+(const char* a, const String& b);

		/**
		 * Retrieves the character at the specified index. Undefined results if index is out of bounds.
		 * @param index The index of the string
		 * @return char The character at the index
		 */
		char operator[](uint32 index) const;

		/**
		 * Overloaded dereference operator to retieve the c string representation.
		 * @return char* The c string representation
		 */
		const char* operator*() const;

		/**
		 * Splits a string up into substrings that were delimited by the specified parameter.
		 *
		 * @param str The delimiter to use to split the string.
		 * @param result A pointer to an uninstantiated array.
		 * @return int32 The count of strings that resulted from the split operation.
		 */
		int32 split(const String& str, String** result);

		/**
		 * Retrieves a substring contained within this string.
		 *
		 *
		 * @param beg The beginning index of the string.
		 * @param end The ending index of the substring, non inclusive of the element at that index.
		 * @return String the substring contained within this string.
		 */
		String substring(uint32 beg, uint32 end) const;

		/**
		 * Returns the right most specified amount of characters.
		 * @param count The amount of characters
		 * @return The resulting string
		 */
		String right_most(uint32 count) const;
		
		/**
		 * Returns the left most specified amount of characters.
		 * @param count The amount of characters
		 * @return The resulting string
		 */
		String left_most(uint32 count) const;

		/**
		 * Finds the first occurance of a substring.
		 * @param str The substring to search for
		 * @param index The index starting from the left from which to start the search.
		 * @return int32 The index of where the substring occurs, or -1 if it does not occur
		 */
		int32 find_first(const String& str, uint32 index) const;

		/**
		 * Finds the last occurance of a substring.
		 * @param str The substring to search for
		 * @param index The index starting from the right from which to start the search.
		 * @return int32 The index of where the substring occurs, or -1 if it does not occur
		 */
		int32 find_last(const String& str, uint32 index) const;

	private:

		char* data;
		uint32 size;

	};

	ENGINE_API String to_string(int64 a);
	ENGINE_API String to_string(uint64 a);
	ENGINE_API String to_string(uint32 a);
	ENGINE_API String to_string(int32 a);
	ENGINE_API String to_string(double a);
	ENGINE_API String to_string(float a);
	ENGINE_API String to_string(bool a);
}

// Cross-DLL memory compatibility
ENGINE_API void FreeArray(Ry::String* arrayPtr);