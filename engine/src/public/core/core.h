#pragma once

#include <ostream>

/************************************************************************/
/* Platform types                                                       */
/************************************************************************/
typedef long long int int64;
typedef int int32;
typedef short int int16;
typedef unsigned long long int uint64;
typedef unsigned int uint32;
typedef unsigned short int uint16;
typedef unsigned char uint8;


/************************************************************************/
/* API defines                                                          */
/************************************************************************/
#ifdef COMPILE_DLL
	#define ENGINE_API __declspec(dllexport)
#else
	#define ENGINE_API __declspec(dllimport)
#endif

/************************************************************************/
/* Assets                                                               */
/************************************************************************/
#ifdef _DEBUG
#define CORE_ASSERT(cond, ...) if(!(cond)) {printf(__VA_ARGS__); abort();}
#else
#define CORE_ASSERT(cond, ...)
#endif

/************************************************************************/
/* Smart allocation                                                     */
/************************************************************************/

/**
 * The maximum size an allocation can be before it will be allocated on the heap.
 */
#define STACK_ALLOC_LIMIT 1024

/**
* Allocates on the stack if below a certain memory requirement threshold and on the heap in all other cases.
* CORE_DELETE must be called after this to free up the allocated memory.
*/
#define CORE_ALLOC(dst, type, count) \
if(sizeof(type) * (count) < STACK_ALLOC_LIMIT) \
{ \
	dst = (type*) alloca(sizeof(type) * count); \
} \
else \
{ \
	dst = (type*) malloc(sizeof(type) * count); \
}

/**
 * Meant to complement CORE_ALLOC, frees up memory that was allocated by core alloc.
 */
#define CORE_DELETE(dst, type, count) \
if(sizeof(type) * (count) >= STACK_ALLOC_LIMIT) \
{ \
	delete dst; \
}
