#pragma once

#include "application.h"
#include "core/core.h"
#include "core/string.h"

#define GAME_CONFIG "./game.ini"

struct GLFWwindow;
struct GLFWmonitor;

// Input callbacks
void error_callback(int error, const char* desc);
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
void button_callback(GLFWwindow* window, int button, int action, int mods);
void scroll_wheel_callback(GLFWwindow* window, double x_off, double y_off);
void char_callback(GLFWwindow* window, unsigned int codepoint);

// Window callbacks
void window_size_callback(GLFWwindow* window, int width, int height);

namespace Ry
{

	struct WindowConfig
	{
		String title;
		int32 width;
		int32 height;
		bool fullscreen;
		int32 monitor;
	};

	struct DesktopConfig
	{
		int fps_lock;
		bool busy;
		AbstractGame* game;
		WindowConfig window;

		DesktopConfig();
		DesktopConfig(const DesktopConfig& config);

		DesktopConfig& operator=(const DesktopConfig& config);

	};

	class DesktopApp : public Application
	{
	public:

		DesktopApp(AbstractGame* game, Ry::String app_name);
		virtual ~DesktopApp();

		virtual void run();
		virtual void stop();

		virtual int32 get_width() const;
		virtual int32 get_height() const;
		virtual int32 get_fps() const;

		ENGINE_API bool is_fullscreen() const;
		ENGINE_API void set_fullscreen(bool fullscreen);
		ENGINE_API void set_fullscreen(bool fullscreen, int32 monitor);

	private:

		GLFWmonitor* get_monitor_by_index(int32 index) const;

		// Backup when transitioning between fullscreen/windowed
		int32 fullscreen_monitor;
		int32 window_pos[2];
		int32 window_size[2];

		// GLFW window
		GLFWwindow* window;

		int frames;
		int fps;
		bool initialized;

		void init_engine();
		void init_resources();
		void init_window();
		void init_renderer();

 		void save_config(const Ry::String& name);
 		void load_config(const Ry::String& name);

		void update(float delta);
		void render();

		void exit();

		DesktopConfig config;
	};

}