#pragma once

#include "core/core.h"
#include "math/math.h"

namespace Ry
{

	/**
	 * Translate, rotation, scale transform.
	 */
	class ENGINE_API Transform
	{

	public:

		/**
		 * The translation of the transform.
		 */
		Vector3 position = Vector3(0.0f, 0.0f, 0.0f);

		/**
		 * Each component defines the corresponding axis of rotation. X-Axis rotation is roll,
		 * Y-Axis rotation is pitch, and Z-Axis rotation is yaw.
		 */
		Vector3 rotation = Vector3(0.0f, 0.0f, 0.0f);
		
		/**
		 * The scale of the transform.
		 */
		Vector3 scale = Vector3(1.0f, 1.0f, 1.0f);

		Transform()
		{

		}

		Vector3 get_forward() const;

		Vector3 get_right() const;

		Vector3 get_up() const;

		Matrix4 get_rotation_mat() const;

		/**
		 * Gets the matrix representation of this transform.
		 */
		Matrix4 get_transform() const;

		/**
		 * Gets the matrix representation of this transform in the opposite direction.
		 */
		Matrix4 get_inverse_transform() const;

	};

	Transform linear_interp(const Transform& a, const Transform& b, float delta);

}