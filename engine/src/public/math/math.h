#pragma once

#include "core/core.h"
#include "vector.h"
#include "matrix.h"

#define PI 3.14159f
#define DEG_TO_RAD(DEG) (DEG * (PI / 180.0f))
#define RAD_TO_DEG(RAD) (RAD * (180.0f / PI))

namespace Ry
{

	Ry::Matrix4 solve_system(Ry::Vector4 a[12], Ry::Vector4 b[12]);
}