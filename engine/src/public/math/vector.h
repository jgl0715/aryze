#pragma once

#include "core/core.h"
#include "core/string.h"
#include "core/string.h"
#include <cstdarg>
#include <sstream>
#include <iomanip>

namespace Ry
{
	template <uint32 S>
	class ENGINE_API Vector
	{

	public:

		float& x;
		float& y;
		float& z;
		float& w;
		float& r;
		float& g;
		float& b;
		float& a;

		Vector();
		Vector(float p_x);
		Vector(float p_x, float p_y);
		Vector(float p_x, float p_y, float p_z);
		Vector(float p_x, float p_y, float p_z, float p_w);

		Vector(const Vector& o);

		~Vector();

		Vector& operator-=(const Vector& o);
		Vector& operator+=(const Vector& o);
		Vector& operator*=(const Vector& o);
		Vector& operator*=(float s);
		Vector operator-(const Vector& o) const;
		Vector operator+(const Vector& o) const;
		float operator*(const Vector& o) const;
		Vector operator*(float s) const;
		float& operator[](uint32 index);
		float operator[](uint32 index) const;

		Vector& operator=(const Vector& o);
		const float* operator*() const;

		template<uint32 S>
		friend String to_string(const Vector<S>& vec);

		template<uint32 S>
		friend float magnitude(const Vector<S>& vec);

		template<uint32 S>
		friend Vector<S>& normalize(Vector<S>& vec);

		template<uint32 S>
		friend Vector<S> normalized(const Vector<S>& vec);

		template<uint32 S>
		friend float dot(const Vector<S>& a, const Vector<S>& b);

		ENGINE_API friend Vector<3> cross(const Vector<3>& a, const Vector<3>& b);
		ENGINE_API friend Vector<3> rot_vec(const Vector<3>& vec, const Vector<3>& axis, float theta);
		ENGINE_API friend Vector<3> make_rot(const Vector<3>& forward);
		ENGINE_API friend float angle(const Vector<3>& a, const Vector<3>& b);

	private:

		float data[S];
	};

	// Vector shorthands
	typedef Vector<4> Vector4;
	typedef Vector<3> Vector3;
	typedef Vector<2> Vector2;

}