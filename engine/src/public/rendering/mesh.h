#pragma once

#include "core/core.h"
#include "math/math.h"
#include "rendering/rendering.h"
#include "rendering/material.h"
#include <unordered_map>

namespace Ry
{

	class VertexBuffer;
	struct VertexAttrib;

	class Shader;
	class VertexArray;

	struct ENGINE_API MeshSection
	{
		int32 section_id;
		int32 start_index;
		int32 count;
		Material* material;
		Shader* shader;
	};

	/**
	 * A mesh is a collection of vertices and indexes defining either a 3D or 2D object.
	 */
	class Mesh
	{
	public:

		/**
		 * Creates a blank mesh.
		 * @param maxVerts Determines how much space to allocate to store vertices
		 * @param maxIndices Determines how much space to allocate to store indices
		 * @param usage The intended usage of this mesh
		 */
		ENGINE_API Mesh(BufferHint usage = BufferHint::STATIC);

		/**
		 * Creates a blank mesh.
		 * @param maxVerts Determines how much space to allocate to store vertices
		 * @param maxIndices Determines how much space to allocate to store indices
		 * @param usage The intended usage of this mesh
		 * @param attribs The vertex attributes to use for each vertex in this mesh.
		 * @param attribCount The amount of vertex attributes
		 */
		ENGINE_API Mesh(const VertexFormat& format, BufferHint usage = BufferHint::STATIC);
	
		ENGINE_API ~Mesh();

		/**
		 * Deletes the resources internal to this mesh, not including the shader it is currently using. Do not use this mesh after calling this.
		 */
		ENGINE_API void deleteMesh();

		/**
		 * @return uint32 The amount of vertices in this mesh
		 */
		ENGINE_API uint32 get_vert_count() const;

		/**
		 * @return uint32 The amount of indices in this mesh
		 */
		ENGINE_API uint32 get_index_count() const;

		ENGINE_API void new_section();
		ENGINE_API Material* get_material(int32 slot);
		ENGINE_API void set_material(int32 slot, Material* mat);
		ENGINE_API Shader* get_shader(int32 slot);
		ENGINE_API void set_shader_all(Shader* shader);
		ENGINE_API void set_shader(int32 slot, Shader* shader);
		ENGINE_API int32 get_current_section_index() const;

		/**
		 * Sets the shader that is used to render this mesh.
		 * @param shader The shader
		 */
// 		ENGINE_API void set_shader(Shader* shader);

		/**
		 * Adds a vertex to this mesh. Once finished adding vertices and indices, make sure to call update()
		 * @param vertex The vertex
		 */
		ENGINE_API void add_vertex(const Vertex* vert);
		ENGINE_API void add_vertex(float x, float y, float z, float u, float v, float r, float g, float b, float a);
		ENGINE_API void add_vertex(float* data);

		/**
		 * Adds an index to this mesh. Once finished adding vertices and indices, make sure to call update()
		 * @param uint32 The mesh index
		 */
		ENGINE_API void add_index(uint32 index);

		/**
		 * Adds two indices to this mesh.
		 * @param uint32 The first mesh index
		 * @param uint32 The second mesh index
		 */
		ENGINE_API void add_line(uint32 i1, uint32 i2);

		/**
		 * Adds two indices to this mesh.
		 * @param i1 The first mesh index
		 * @param i2 The second mesh index
		 * @param i3 The third mesh index
		 */
		ENGINE_API void add_tri(uint32 i1, uint32 i2, uint32 i3);

		/**
		 * Updates the GPU vertex and index data. This is the sole function that mutates mesh data on the GPU. Must be called after previously calling add_vertex and add_index
		 */
		ENGINE_API void update();

		/**
		 * Renders the data contained in this mesh.
		 * @prim The primitive to use for rendering the mesh.
		 */
		ENGINE_API void render(Primitive prim);

		/**
		 * Clears the vertex and index data internally stored in this mesh. Data on the GPU remains the same until update is called. O(1) time complexity.
		 */
		ENGINE_API void clear();

	private:

		int32 vert_count;
		int32 index_count;

		int32 max_section_id;
		std::unordered_map<int32, MeshSection> sections;

		// Store vertex and index information of the mesh.
		std::vector<float> verts;
		std::vector<uint32> indices;

		VertexArray* vao;
		BufferHint hint;
	};

	ENGINE_API Mesh* load_obj(const Ry::String mesh_path, const Ry::String material_path);
}