#pragma once

#include "texture.h"

namespace Ry
{

	class ENGINE_API FrameBuffer
	{

	public:

		FrameBuffer()
		{

		}

		~FrameBuffer()
		{

		}

		virtual void bind() = 0;
		virtual void unbind() = 0;
		virtual void setup() = 0;
		virtual void attach_color_texture(Texture* texture, int32 index) = 0;
		virtual bool check_status() = 0;

	};
}
