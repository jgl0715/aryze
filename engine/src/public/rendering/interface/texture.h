#pragma once

#include "core/core.h"

namespace Ry
{

	/**
	 * Specifies a pixel format for textures.
	 */
	enum class ENGINE_API PixelFormat
	{
		RGB, RGBA, RED
	};

	class ENGINE_API Texture
	{

	public:

		/************************************************************************/
		/* Interface functions                                                  */
		/************************************************************************/

		/**
		 * Deletes this texture from graphics memory. This texture should not be used after calling this.
		 */
		virtual void deleteTexture() = 0;

		/**
		 * Binds this texture for rendering usage. Should be called before rendering a vertex array.
		 */
		virtual void bind() = 0;

		/**
		 * Unbinds any currently bound texture.
		 */
		virtual void unbind() = 0;

		/**
		 * Sets this texture up as a blank buffer with the specified width and height.
		 * @param width The width of the texture.
		 * @param height The height of the texture.
		 */
		virtual void data(uint32 width, uint32 height) = 0;

		/**
		 * Uploads data to this texture.
		 * @param data The float data to store in this texture.
		 * @param format The pixel format of the data
		 * @param width The width of the data
		 * @param height The height of the data
		 */
		virtual void data(float* data, PixelFormat format, uint32 width, uint32 height) = 0;

		/**
		 * Uploads data to this texture.
		 * @param data The byte data to store in this texture.
		 * @param format The pixel format of the data
		 * @param width The width of the data
		 * @param height The height of the data
		 */
		virtual void data(uint8* data, PixelFormat format, uint32 width, uint32 height) = 0;
	};
}
