#pragma once

#include "core/core.h"
#include "math/math.h"

#include "interface/vertexarray.h"
#include "interface/shader.h"
#include "interface/texture.h"
#include "interface/framebuffer.h"
#include "rendering/vertex.h"
#include "rendering/mesh.h"

#include "core/string.h"

namespace Ry
{

	/**
	 * All supported rendering platforms.
	 */
	enum class RenderingPlatform
	{
		GL4
	};

	enum class RenderMode
	{
		FILLED, WIREFRAME
	};

	/**
	 * Represents an abstract rendering interface.
	 */
	class Renderer
	{
	public:

		/**
		 * Sets the clear color for the screen.
		 * @param The clear color of the screen
		 */
		virtual void set_clear_color(const Vector4& color) = 0;
		
		/**
		 * Clears the color buffer of the screen.
		 */
		virtual void clear_screen() = 0;

		virtual void set_render_mode(const RenderMode& mode) = 0;
		
		virtual void set_viewport(int32 x, int32 y, int32 width, int32 height) = 0;

		/**
		 * Makes a new vertex array using the correct rendering API.
		 */
		virtual VertexArray* make_vertex_array(const VertexFormat& format, bool with_index_buffer) = 0;

		/**
		 * Makes a new shader using the correct rendering API.
		 * @param vertName The name of the vertex shader to use
		 * @param fragName The name of the fragment shader to use
		 */
		virtual Shader* make_shader(const VertexFormat& format, const Ry::String& vertName, const Ry::String& fragName) = 0;
		
		/**
		 * Makes a texture using the correct rendering API.
		 */
		virtual Texture* make_texture() = 0;

		virtual FrameBuffer* make_framebuffer() = 0;

	};

}