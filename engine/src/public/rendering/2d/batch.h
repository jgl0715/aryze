#pragma once

#include "core/core.h"
#include "math/math.h"

#define SHAPE_VERT "base.glv"
#define SHAPE_FRAG "single_shade.glf"
#define TEX_VERT "base.glv"
#define TEX_FRAG "texture_tint.glf"

#define MAX_VERTS 50000
#define MAX_INDICES 300000

#define CHECK_FLUSH_PRIM(primitive, needed_verts, needed_indices)  \
if (primitive != this->prim || mesh->get_vert_count() + needed_verts >= MAX_VERTS || mesh->get_index_count() + needed_indices >= MAX_INDICES) \
{ \
	flush(); \
	this->prim = primitive; \
} \


#define CHECK_FLUSH_TEXTURE(text)  \
if (text != this->texture || mesh->get_vert_count() + 4 >= MAX_VERTS || mesh->get_index_count() + 6 >= MAX_INDICES) \
{ \
	flush(); \
	this->texture = text; \
} \

#define CHECK_ENDED() \
if(began) \
{ \
	std::cerr << "Must invoke end() before invoking a subsequent begin()" << std::endl; \
	return; \
}

#define CHECK_BEGAN() \
if(!began) \
{ \
	std::cerr << "Must invoke begin() before calling rendering functions" << std::endl; \
	return; \
}

#define ADD_LINE(v1, v2) \
uint32 i0=mesh->get_vert_count(); \
mesh->add_vertex(&v1); \
mesh->add_vertex(&v2); \
mesh->add_index(i0); \
mesh->add_index(i0+1); \

#define ADD_TRI(v1, v2, v3) \
uint32 i0 = mesh->get_vert_count(); \
mesh->add_vertex(&v1); \
mesh->add_vertex(&v2); \
mesh->add_vertex(&v3); \
mesh->add_tri(i0, i0+1, i0+2); \

#define ADD_QUAD(v1, v2, v3, v4) \
uint32 i0 = mesh->get_vert_count(); \
mesh->add_vertex(&v1); \
mesh->add_vertex(&v2); \
mesh->add_vertex(&v3); \
mesh->add_vertex(&v4); \
mesh->add_tri(i0, i0+1, i0+2); \
mesh->add_tri(i0+2, i0+3, i0+0); \

#define BATCH_VARS() \
bool began; \
Shader* shader; \
Matrix4 proj; \
Matrix4 view; \
Mesh* mesh; \

namespace Ry
{

	enum class Primitive;
	class Mesh;
	class Camera;
	class Shader;
	class Texture;

	/**
	 * Draw modes for 2D batches.
	 */
	enum ENGINE_API DrawMode
	{
		FILLED, OUTLINE
	};

	/**
	 * Batches 2D shapes.
	 */
	class ENGINE_API ShapeBatch
	{
	public:

		ShapeBatch();
		~ShapeBatch();

		/**
		 * Starts the rendering process. Begin can not be called subsequently and end() must follow each begin.
		 */
		void begin(DrawMode mode);

		void draw_hollow_circle(float x, float y, float r, uint32 segments);
		void draw_filled_circle(float x, float y, float r, uint32 segments);
		void draw_rect(float x, float y, float w, float h);
		void draw_hollow_rect(float x, float y, float w, float h);
		void draw_line(float x1, float y1, float x2, float y2);
		
		/**
		 * Batches all shapes that are currently built up in the buffer.
		 */
		void flush();

		/**
		 * Ends the rendering cycle for this frame. Must follow each begin and can not be called subsequently.
		 */
		void end();

		/**
		 * Sets the tint color of this shape batch.
		 * @param color The tint color
		 */
		void set_color(const Vector4& color);

		/**
		 * Sets the projection matrix to use for this shape batch.
		 * @param proj The projection matrix
		 */
		void set_projection(const Matrix4& proj);

		/**
		 * Sets the projection matrix to use for this shape batch.
		 * @param view The view matrix
		 */
		void set_view(const Matrix4& view);

		/**
		 * Sets up the correct matrices for rendering based on the camera.
		 * @param cam The camera
		 */
		void update(const Camera* cam);

	private:

		BATCH_VARS()

		Primitive prim;
		DrawMode mode;
		Vector4 color;
	};


	class ENGINE_API TextureBatch
	{
	public:

		TextureBatch();
		~TextureBatch();

		/**
		 * Starts the rendering process. Begin can not be called subsequently and end() must follow each begin.
		 */
		void begin();
		
		/************************************************************************/
		/* Texture rendering functions                                          */
		/************************************************************************/

		void draw_texture(Texture* texture, float x, float y, float w, float h);
		
		/**
		* Batches all textures that are currently built up in the buffer.
		*/
		void flush();

		/**
		 * Ends the rendering cycle for this frame. Must follow each begin and can not be called subsequently.
		 */
		void end();

		/**
		 * Sets the shader to use for rendering textures.
		 * @param shader The shader
		 */
		void set_shader(Shader* shader);

		/**
		 * Sets the tint color.
		 * @param color The tint color
		 */
		void set_color(const Vector4& color);

		/**
		 * Sets the projection matrix to use for this shape batch.
		 * @param proj The projection matrix
		 */
		void set_projection(const Matrix4& proj);

		/**
		 * Sets the projection matrix to use for this shape batch.
		 * @param view The view matrix
		 */
		void set_view(const Matrix4& view);

		/**
		 * Sets up the correct matrices for rendering based on the camera.
		 * @param cam The camera
		 */
		void update(const Camera* cam);

	private:

		BATCH_VARS()

		Texture* texture;
		Vector4 color;

	};
}