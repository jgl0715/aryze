#pragma once

#include "core/core.h"
#include "rendering/interface/shader.h"
#include "math/math.h"

namespace Ry
{
	struct ENGINE_API Material
	{
		Ry::String name;
		Vector3 ambient_color;
		Vector3 diffuse_color;
		Vector3 specular_color;
	};
}
