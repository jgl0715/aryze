#pragma once

#include "core/core.h"
#include "core/string.h"
#include "math/math.h"

namespace Ry
{

	/**
	 * Simple vertex storing only position.
	 */
	struct ENGINE_API Vertex
	{
		virtual void populate(float* data) const = 0;
	};

	struct ENGINE_API Vertex1P : public Vertex
	{
		Vector3 pos;

		Vertex1P():
			Vertex1P(0.0f, 0.0f, 0.0f)
		{

		}

		Vertex1P(float x, float y, float z)
		{
			pos.x = x;
			pos.y = y;
			pos.z = z;
		}

		virtual void populate(float* data) const;
	};

	/**
	 * Vertex storing position and color attributes.
	 */
	struct ENGINE_API Vertex1P1C : public Vertex1P
	{
		Vector4 color;

		Vertex1P1C():
			Vertex1P1C(0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f)
		{

		}

		Vertex1P1C(float x, float y, float z, float r, float g, float b, float a) :
			Vertex1P(x, y, z)
		{
			color.r = r;
			color.g = g;
			color.b = b;
			color.a = a;
		}

		virtual void populate(float* data) const;
	};

	/**
	 * Vertex storing position and texture coordinate attributes.
	 */
	struct ENGINE_API Vertex1P1UV : public Vertex1P
	{
		Vector2 tex_coord;

		Vertex1P1UV():
			Vertex1P1UV(0.0f, 0.0f, 0.0f, 0.0f, 0.0f)
		{

		}

		Vertex1P1UV(float x, float y, float z, float u, float v) :
			Vertex1P(x, y, z)
		{
			tex_coord.x = u;
			tex_coord.y = v;
		}

		virtual void populate(float* data) const;
	};

	/**
	 * Vertex storing position, texture coordinate, and color attributes.
	 */
	struct ENGINE_API Vertex1P1UV1C : public Vertex1P1UV
	{
		Vector4 color;

		Vertex1P1UV1C():
			Vertex1P1UV1C(0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f)
		{

		}

		Vertex1P1UV1C(float x, float y, float z, float u, float v, float r, float g, float b, float a) :
			Vertex1P1UV(x, y, z, u, v)
		{
			color.r = r;
			color.g = g;
			color.b = b;
			color.a = a;
		}

		virtual void populate(float* data) const;
	};

	/**
	 * Defines meta information about a vertex attribute. A vertex attribute is an atomic element of a vertex.
	 */
	struct ENGINE_API VertexAttrib
	{
		/**
		 * The name of the vertex attribute.
		 */
		String name;

		/**
		 * The amount of elements in the vertex attribute, usually floats.
		 */
		int32 size;

		VertexAttrib()
		{
			name = "";
			size = 0;
		}

		VertexAttrib(const String& n, int32 si)
		{
			name = n;
			size = si;
		}

		bool operator==(const VertexAttrib& other);
	};

	struct ENGINE_API VertexFormat
	{
		VertexAttrib* attributes;
		int32 attribute_count;
		int32 element_count;

		VertexFormat():
			VertexFormat(nullptr, 0)
		{

		}

		VertexFormat(VertexAttrib* attributes, int32 attrib_count) :
			attributes(attributes),
			attribute_count(attrib_count),
			element_count(0)
		{
			for (int32 i = 0; i < attribute_count; i++)
			{
				element_count += attributes[i].size;
			}
		}
	};

	/**
	* Stores a vertex attribute with its relative offset in a series of vertex attributes.
	*/
	struct ENGINE_API VertexAttribPtr
	{
		VertexAttrib attrib;
		int32 offset;
	};

	extern ENGINE_API VertexAttrib pos;
	extern ENGINE_API VertexAttrib color;
	extern ENGINE_API VertexAttrib uv;
	extern ENGINE_API VertexAttrib normal;
	extern ENGINE_API VertexAttrib tangent;
	extern ENGINE_API VertexAttrib bitangent;

	// position
	extern ENGINE_API VertexAttrib ATTRIB_ARRAY_1P[1];
	
	// position, color
	// position, uv
	// position, normal
	extern ENGINE_API VertexAttrib ATTRIB_ARRAY_1P1C[2];
	extern ENGINE_API VertexAttrib ATTRIB_ARRAY_1P1UV[2];
	extern ENGINE_API VertexAttrib ATTRIB_ARRAY_1P1N[2];

	// position, uv, color
	// position, color, uv
	// position, uv, normal
	// position, normal, uv
	// position, color, normal
	// position, normal, color
	extern ENGINE_API VertexAttrib ATTRIB_ARRAY_1P1UV1C[3];
	extern ENGINE_API VertexAttrib ATTRIB_ARRAY_1P1C1UV[3];
	extern ENGINE_API VertexAttrib ATTRIB_ARRAY_1P1UV1N[3];
	extern ENGINE_API VertexAttrib ATTRIB_ARRAY_1P1N1UV[3];
	extern ENGINE_API VertexAttrib ATTRIB_ARRAY_1P1C1N[3];
	extern ENGINE_API VertexAttrib ATTRIB_ARRAY_1P1N1C[3];

	extern ENGINE_API VertexFormat VF1P;
	extern ENGINE_API VertexFormat VF1P1C;
	extern ENGINE_API VertexFormat VF1P1UV;
	extern ENGINE_API VertexFormat VF1P1UV1C;
	extern ENGINE_API VertexFormat VF1P1C1UV;
	extern ENGINE_API VertexFormat VF1P1UV1N;
	extern ENGINE_API VertexFormat VF1P1N1UV;
	extern ENGINE_API VertexFormat VF1P1C1N;
	extern ENGINE_API VertexFormat VF1P1N1C;

}