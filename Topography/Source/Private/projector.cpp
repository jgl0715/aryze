#include "projector.h"
#include <iostream>
#include "worker.h"
#include <thread>
#include <mutex>
#include <profiler.h>
#include <rendering/mesh.h>
#include <rendering/2d/batch.h>
#include "keys.h"
#include <rendering/interface/shader.h>
#include "globals.h"
#include <util.h>
#include "file/file.h"

ARSandbox::ARSandbox()
{

}

void ARSandbox::init()
{
	Ry::input_handler->addKeyListener(this);

	calibrating = false;

	load_calibration(plane_calib, kinect_calib, projector_calib);
//	kinect_calib.x = 0;
//	kinect_calib.y = 0;
//	kinect_calib.w = 512;
//	kinect_calib.h = 424;

	std::cout << "Loaded calibration: " << kinect_calib.x << " " << kinect_calib.y << " " << kinect_calib.w << " " << kinect_calib.h << std::endl;

//	worker = new PerlinSandboxWorker(kinect_calib.w, kinect_calib.h);
	worker = make_worker_from_calibration(kinect_calib);

	cam = new Ry::PerspectiveCamera(4.0f / 3.0f, 78.68f, 0.1f, -100.0f);
	cam->transform.position.z = 5.0f;

	// Setup textures
	hm_texture = Ry::rapi->make_texture();
	terrain_texture = Ry::rapi->make_texture();
	terrain_texture->data(Ry::app->get_width(), Ry::app->get_height());

	// Setup terrain shader.
	terrain_color_shader = Ry::rapi->make_shader(Ry::VF1P1UV1C, "topography_color.glv", "topography_color.glf");
	terrain_contour_shader = Ry::rapi->make_shader(Ry::VF1P1UV1C, "topography_contour.glv", "topography_contour.glf");

	// Setup meshes
	terrain_mesh = new Ry::Mesh(Ry::VF1P1UV1C);

	std::this_thread::sleep_for(std::chrono::duration<int>(1));

	// Generate a mesh that is the same resolution as the height map
//	float scale_x = 1 / 12.8f;
//	float scale_y = -1 / 12.8f;
//	float hw = kinect_calib.w / 2.0f;
//	float hh = kinect_calib.h / 2.0f;
	for (int32 j = 0; j < kinect_calib.h; j++)
	{
		for (int32 i = 0; i < kinect_calib.w; i++)
		{
			float value = 0.0f;

			int32 ax = i + kinect_calib.x;
			int32 ay = j + kinect_calib.y;

			// The z value will not be used until later, we only care about x and y value
			Ry::Vector3 camera_space = worker->get_camera_space(ax, ay);

			if(i % 500000 == 0)
			{
				std::cout << camera_space.y << std::endl;
			}
			
			//camera_space.x = (i - hw / 2) * scale_x;// (((float)i / (kinect_calib.w - 1)) - 0.5f) * 2.0f;
			//camera_space.y = (j - hh / 2) * scale_y;// (((float)j / (kinect_calib.h - 1)) - 0.5f) * 2.0f;

			camera_space.z = 0.0f;
			float u = (float) i / (kinect_calib.w-1);
			float v = (float) j / (kinect_calib.h-1);

			Ry::Vertex1P1UV1C vertex(camera_space.x, camera_space.y, camera_space.z, u, v, 1.0f, 1.0f, 1.0f, 1.0f);
			terrain_mesh->add_vertex(&vertex);

			if (i < kinect_calib.w - 1 && j < kinect_calib.h - 1)
			{
				int32 top_left = (j + 0) * kinect_calib.w + (i + 0);
				int32 top_right = (j + 0) * kinect_calib.w + (i + 1);
				int32 bottom_left = (j + 1) * kinect_calib.w + (i + 0);
				int32 bottom_right = (j + 1) * kinect_calib.w + (i + 1);

				terrain_mesh->add_tri(top_left, top_right, bottom_right);
				terrain_mesh->add_tri(bottom_right, bottom_left, top_left);
			}
		}
	}
	terrain_mesh->update();
	terrain_mesh->set_shader(0, terrain_color_shader);

	// Construct a screen space mesh
	screen_space_mesh = new Ry::Mesh(Ry::VF1P1UV1C);
	screen_space_mesh->add_vertex(-1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f);
	screen_space_mesh->add_vertex(-1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f);
	screen_space_mesh->add_vertex(1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f);
	screen_space_mesh->add_vertex(1.0f, -1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f);
	screen_space_mesh->add_tri(0, 1, 2);
	screen_space_mesh->add_tri(2, 3, 0);
	screen_space_mesh->update();
	screen_space_mesh->set_shader(0, terrain_contour_shader);

	// Init terrain fbo
	terrain_fbo = Ry::rapi->make_framebuffer();
	terrain_fbo->bind();
	{
		terrain_fbo->attach_color_texture(terrain_texture, 0);
		terrain_fbo->setup();
		if (!terrain_fbo->check_status())
		{
			std::cout << "FBO failed to setup" << std::endl;
		}
		else
		{
			std::cout << "FBO setup properly" << std::endl;
		}
	}
	terrain_fbo->unbind();
	
}

void ARSandbox::update(float delta)
{
	worker->begin();
	hm_texture->data(worker->get_data(), Ry::PixelFormat::RED, kinect_calib.w, kinect_calib.h);
	worker->end();
}

void ARSandbox::render()
{

	float speed = 0.01f;

	if (Ry::input_handler->isKeyDown(KEY_LEFT_CONTROL))
	{
		cam->transform.position.z -= speed;

//		scale -= 0.003f;
	}
	else if (Ry::input_handler->isKeyDown(KEY_LEFT_SHIFT))
	{
		cam->transform.position.z += speed;

//		scale += 0.001f;
	}

	if (Ry::input_handler->isKeyDown(KEY_A))
	{
		cam->transform.position.x -= speed;

//		offset_x -= 0.001f;
	}
	else if (Ry::input_handler->isKeyDown(KEY_D))
	{
		cam->transform.position.x += speed;

//		offset_x += 0.001f;
	}
	if (Ry::input_handler->isKeyDown(KEY_S))
	{
		cam->transform.position.y -= speed;
//		offset_y -= 0.001f;
	}
	else if (Ry::input_handler->isKeyDown(KEY_W))
	{
		cam->transform.position.y += speed;
//		offset_y += 0.001f;
	}
	if (Ry::input_handler->isKeyDown(KEY_1))
	{
		cam->transform.rotation.z -= speed;
	}
	else if (Ry::input_handler->isKeyDown(KEY_2))
	{
		cam->transform.rotation.z += speed;
	}
	if (Ry::input_handler->isKeyDown(KEY_3))
	{
		cam->transform.rotation.y -= speed;
	}
	else if (Ry::input_handler->isKeyDown(KEY_4))
	{
		cam->transform.rotation.y += speed;
	}
	if (Ry::input_handler->isKeyDown(KEY_5))
	{
		cam->transform.rotation.x -= speed;
	}
	else if (Ry::input_handler->isKeyDown(KEY_6))
	{
		cam->transform.rotation.x += speed;
	}
	if (Ry::input_handler->isKeyDown(57))
	{
		cam->fov += 0.1f;
		cam->update();
	}
	else if (Ry::input_handler->isKeyDown(48))
	{
		cam->fov -= 0.1f;
		cam->update();
	}

	// Render the base topography color to a texture.
	terrain_fbo->bind();
	Ry::rapi->set_viewport(0, 0, Ry::app->get_width(), Ry::app->get_height());
	{
		hm_texture->bind();
		{
//			terrain_color_shader->uniform_float("scale", scale);
//			terrain_color_shader->uniform_float("rotation", rotation);
//			terrain_color_shader->uniform_float("offset_x", offset_x);
//			terrain_color_shader->uniform_float("offset_y", offset_y);

			terrain_color_shader->uniformMat44("view_proj", projector_calib.view_projection);
		

			//float range = ((float) worker->get_max() - worker->get_min()) / 1000.0f;
			terrain_color_shader->uniform_float("height_min", worker->get_min() / 1000.0f);
			terrain_color_shader->uniform_float("height_max", worker->get_max() / 1000.0f);

			terrain_mesh->render(Ry::Primitive::TRIANGLE);
		}
	}
	terrain_fbo->unbind();

	terrain_texture->bind();
	{
		terrain_contour_shader->uniform_float("width", (float) Ry::app->get_width());
		terrain_contour_shader->uniform_float("height", (float) Ry::app->get_height());
		
		screen_space_mesh->render(Ry::Primitive::TRIANGLE);
	}

// 	terrain_shader->uniform_float("u_width", kinect_calib.w);
// 	terrain_shader->uniform_float("u_height", kinect_calib.h);

//	hm_texture->bind();
//	terrain_mesh->render(Ry::Primitive::TRIANGLE);
}

void ARSandbox::quit()
{
	worker->stop();
	delete worker;
}

void ARSandbox::onKeyPressed(int32 key)
{
	if (key == KEY_ENTER)
	{
		if (calibrating)
		{
			save_calib();
			calibrating = false;
		}
		else
		{
			calibrating = true;
		}
	}
}

void ARSandbox::onKeyReleased(int32 key)
{

}

void ARSandbox::save_calib()
{
	Ry::String projector_string = to_string(projector_calib.view_projection);
	Ry::String kinect_bounds_string = Ry::to_string(kinect_calib.x) + " " + Ry::to_string(kinect_calib.y) + " " + Ry::to_string(kinect_calib.w) + " " + Ry::to_string(kinect_calib.h);
	Ry::String calib_string = projector_string + kinect_bounds_string;

	Ry::File::write_file("./calibration.dat", calib_string);

	std::cout << "Successfully saved calibration to calibration.dat" << std::endl;
}