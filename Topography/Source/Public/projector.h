#pragma once
#include <application.h>
#include <profiler.h>
#include "sandbox.h"
#include "math/math.h"
#include "rendering/camera.h"
#include "input.h"

namespace Ry
{
	class Mesh;
	class Shader;
	class ShapeBatch;
	class TextureBatch;
	class Texture;
	class FrameBuffer;
}

class KinectSandboxWorker;

class ARSandbox : public Ry::AbstractGame, Ry::KeyListener
{

public:

	ARSandbox();
	virtual void init() override;
	virtual void update(float delta) override;
	virtual void render() override;
	virtual void quit() override;

	void onKeyPressed(int32 key);
	void onKeyReleased(int32 key);

	void save_calib();

private:

	bool calibrating;

	PlaneCalibration plane_calib;
	ProjectorCalibration projector_calib;
	KinectCalibration kinect_calib;

	Ry::Shader* terrain_color_shader;
	Ry::Shader* terrain_contour_shader;

	// Matrices
	Ry::PerspectiveCamera* cam;

	// Meshes
	Ry::Mesh* terrain_mesh;
	Ry::Mesh* screen_space_mesh;

	// Textures
	Ry::Texture* hm_texture;
	Ry::Texture* terrain_texture;

	// Framebuffers
	Ry::FrameBuffer* terrain_fbo;

	Ry::Profiler profiler_single;
	Ry::Profiler profiler_total;

	int32 hm_width;
	int32 hm_height;

	KinectSandboxWorker* worker;
};