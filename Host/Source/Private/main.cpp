#define _SILENCE_EXPERIMENTAL_FILESYSTEM_DEPRECATION_WARNING

#include <iostream>
#include <thread>
#include <mutex>
#include <string>
#include <unordered_map>
#include "application.h"
#include "desktop_app.h"
#include "core/string.h"
#include "globals.h"
#include "net/websocket.h"

#include "Json/json.hpp"
#include "app.h"

#include <experimental/filesystem>

static std::unordered_map<std::string, std::thread*> app_threads;
static std::unordered_map<std::string, Ry::AbstractGame*> app_games;

std::thread* socket_thread;

const std::string motd = R"(

    ___    ____                 
   /   |  / __ \__  ______  ___ 
  / /| | / /_/ / / / /_  / / _ \
 / ___ |/ _, _/ /_/ / / /_/  __/
/_/  |_/_/ |_|\__, / /___/\___/ 
             /____/             

Welcome to the ARyze host software.
To get jump started, type the "help" command.

)";

// websocketpp::server<websocketpp::config::asio> server;

void execute_app(const Ry::String& name, Ry::AbstractGame* game)
{
	Ry::Application* app = Ry::make_application(name, game, Ry::Platform::WINDOWS);
	app->run();
	delete game;
	delete app;
}

Ry::String read_cmd()
{
	std::cout << ">> ";
	std::string cmd;
	std::getline(std::cin, cmd);
	
	// Return canonicalized string
	return Ry::String(cmd.c_str()).to_lower();
}

/*void launch_sandbox()
{
#ifdef INCLUDE_APPS
	Ry::AbstractGame* game = new ARSandbox;
	std::thread* spawned_thread = new std::thread(&execute_app, "Projector", game);

	app_threads["Projector"] = spawned_thread;
	app_games["Projector"] = game;
#endif
}*/

void handle_launch(Ry::String* args, int32 count)
{
	if (count < 1)
	{
		std::cerr << "Incorrect usage: must specify at least one argument for this command." << std::endl;
	}
	else
	{
		Ry::String app = args[0];

		// For now, the apps are hard coded but should later be dynamically loaded.


		// J.Leavell - TODO: Dynamically load apps
		if (app == "sandbox" || app == "sb")
		{
			//launch_sandbox();
		}

		if (app == "visual" || app == "vis")
		{
			//launch_visual();
		}

		if (app == "calibration" || app == "cal")
		{
			//launch_calibration();
		}
	}
}

void handle_apps(Ry::String* args, int32 count)
{
	const Ry::String apps = R"(
sandbox, sb: The main AR sandbox application. This application is in control of rendering to the projector.
visual, vis: The 3D visualization application. This app controls rendering to the monitor and shows an interactible 3D visualization of the current sandbox state.
calibration, cal: This is the calibration tool for the AR sandbox. It is used during setup.
	)";

	std::cout << *apps << std::endl;
}

void handle_help(Ry::String* args, int32 count)
{
	const Ry::String help_msg = R"(
help: Prints out a list of commands and their usage
launch <app_name> options: Launches the app with the specified name and options. Options are not required.
apps: Lists all currently installed apps and their descriptions.
quit, exit, stop: halts the ARyze host
	)";

	std::cout << *help_msg << std::endl;
}

void process_cmd(Ry::String cmd, Ry::String* args, int32 count)
{
	if (cmd == "help")
	{
		handle_help(args, count);
	}
	else if (cmd == "apps")
	{
		handle_apps(args, count);
	}
	else if (cmd == "launch")
	{
		handle_launch(args, count);
	}
}

Ry::WebSocket* server;
Ry::WebSocketConnection* server_connection;

void on_message(Ry::WebSocketConnection* conn, const Ry::String& message)
{
	// J.Leavell - TODO: Dynamically load apps

	
	if (message == "start")
	{
		//launch_sandbox();
	}
	if (message == "calibrate")
	{
		//launch_calibration();
	}
}

std::vector<App> FindApps(const Ry::String& AppDirectory)
{
	std::vector<App> Result;
	std::experimental::filesystem::directory_iterator DirectoryIterator (*AppDirectory);
	
	while (DirectoryIterator != std::experimental::filesystem::end(DirectoryIterator))
	{
		Ry::String ItemPath = DirectoryIterator->path().filename().string().c_str();

		std::cout << "Path: " << *ItemPath << std::endl;
		
		++DirectoryIterator;
	}

	return Result;
}

void run_server()
{
	FindApps(".");

	Ry::String json_string;
	nlohmann::json response;
	nlohmann::json apps_array = nlohmann::json::array();
	response["type"] = "initial";

	App InstalledApps[2] = { CalibrationApp, TopographyApp };
	for(const App& App : InstalledApps)
	{
		nlohmann::json AppJson;
		AppJson["id"] = App.Id;
		AppJson["name"] = *App.Name;
		AppJson["description"] = *App.Description;
		AppJson["version"] = *App.Version;
		AppJson["author"] = *App.Author;

		apps_array.push_back(AppJson);
	}

	json_string = Ry::String(response.dump().c_str());

	//server->send_message(server, json_string);
}

void init_server()
{
	server = Ry::Net::make_websock_server(8080);
	server->add_message_handler(&on_message);

	socket_thread = new std::thread(&run_server);
}

int main()
{
	init_server();

	std::cout << motd << std::endl;
	Ry::String last_cmd = "";

	while (last_cmd != "quit" && last_cmd != "stop" && last_cmd != "exit")
	{
		Ry::String cmd = read_cmd();

		Ry::String* args;
		int32 arg_c = cmd.split(" ", &args);

		if (arg_c > 0)
		{
			if (arg_c > 1)
			{
				// Arguments specified
				process_cmd(args[0], args + 1, arg_c - 1);
			}
			else
			{
				// No arguments specified
				process_cmd(args[0], nullptr, 0);
			}
		}

		last_cmd = cmd;
		delete[] args;
	}

	if (socket_thread)
	{
		socket_thread->join();
		delete socket_thread;
	}

}