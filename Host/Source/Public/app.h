#pragma once

#include "core/string.h"

struct App
{
	Ry::String Name;
	Ry::String Description;
	Ry::String Version;
	Ry::String Author;
	int32 Id;
};

const App CalibrationApp {"Calibration", "Calibrate your sandbox", "0.1", "Joey Leavell"};
const App TopographyApp{ "Topography", "Interactively view topography lines on top of the sand.", "0.1", "Joey Leavell" };