#include <iostream>
#include "math/math.h"
#include "core/string.h"
#include "core/map.h"
#include "application.h"

void vec_dot()
{
	std::cout << "\nTEST VECTOR DOT PRODUCT" << std::endl;

	Ry::Vector3 v1(1.0f, 2.0f, 3.0f);
	Ry::Vector3 v2(4.0f, 5.0f, 6.0f);

	float res = v1 * v2;

	std::cout << res << std::endl;
	std::cout << "END VECTOR DOT PRODUCT\n" << std::endl;
}

void vec_cross()
{
	std::cout << "\nTEST VECTOR CROSS PRODUCT" << std::endl;
	Ry::Vector3 v1(1.0f, 0.0f, 0.0f);
	Ry::Vector3 v2(0.0f, 1.0f, 0.0f);
	Ry::Vector3 v3(0.0f, 1.0f, 0.0f);
	Ry::Vector3 v4(0.0f, 0.0f, 1.0f);
	Ry::Vector3 v5(0.0f, 0.0f, 1.0f);
	Ry::Vector3 v6(1.0f, 0.0f, 0.0f);

	std::cout << *to_string(cross(v1, v2)) << std::endl;
	std::cout << *to_string(cross(v3, v4)) << std::endl;
	std::cout << *to_string(cross(v5, v6)) << std::endl;
	std::cout << "END VECTOR CROSS PRODUCT\n" << std::endl;
}

void vec_add()
{
	std::cout << "\nTEST VECTOR ADD" << std::endl;

	Ry::Vector3 v1(1.0f, 2.0f, 3.0f);
	Ry::Vector3 v2(4.0f, 5.0f, 6.0f);

	Ry::Vector3 res = v1 + v2;

	std::cout << *to_string(res) << std::endl;
	std::cout << "END VECTOR ADD\n" << std::endl;
}

void mat_mult()
{
	std::cout << "\nTEST MAT MULT" << std::endl;

	Ry::Matrix4 a = Ry::id4();
	Ry::Matrix4 b(5.0f);
	Ry::Matrix4 c = b * a;

	std::cout << *to_string(c) << std::endl;
	std::cout << "END MAT MULT\n" << std::endl;
}

void mat_inverse()
{
	std::cout << "\nTEST MAT INV" << std::endl;

	Ry::Matrix3 a = Ry::id3();
	Ry::Matrix3 b(2.0f,1.0f,0.0f,2.0f,0.0f,0.0f,2.0f,0.0f,1.0f);
	Ry::Matrix4 c = Ry::id4();

	std::cout << *to_string(inverse(a)) << std::endl;
	std::cout << *to_string(inverse(b)) << std::endl; // should be 0 0.5 0 1 -1 0 0 -1 1
	std::cout << *to_string(inverse(c)) << std::endl; // should be 0 0.5 0 1 -1 0 0 -1 1

	std::cout << "END MAT INV\n" << std::endl;
}

void string_test()
{
	std::cout << "\nTEST STRING" << std::endl;

	Ry::String a = "abcbcde";
	Ry::String b = "sdf";
	Ry::String c = "bc";
	Ry::String d = "de";
	Ry::String e = "this is a,sentence that is,delimited by,commas";

//	std::cout << a.find_first(b, 3) << std::endl;
//	std::cout << a.find_last(c, 0) << std::endl;
//	std::cout << *a.right_most(6) << std::endl;

	Ry::String* result;
	int32 strings = e.split(",", &result);
	for (int32 i = 0; i < strings; i++)
		std::cout << *result[i] << std::endl;

	delete[] result;

	std::cout << "END STRING\n" << std::endl;
}

void map_test()
{
	//Ry::Map<Ry::String, uint32> map;

	//map.insert("hello", 10);
	//map.insert("b", 16);

	//uint32 hello = *map.get("hello");
	//uint32 b = *map.get("b");

	//std::cout << hello << " " << b << std::endl;
}

int main()
{
	vec_dot();
	vec_add();
	vec_cross();
	mat_mult();
	mat_inverse();
	string_test();
	map_test();

	Ry::String a = "100.3";
	double x = a.to_double() * 2;

	std::cout << x << std::endl;
}