#pragma once

#ifdef COMPILE_DLL
#define KINECT_API __declspec(dllexport)
#else
#define KINECT_API __declspec(dllimport)
#endif

struct IKinectSensor;
struct IDepthFrame;
struct IDepthFrameReader;
struct ICoordinateMapper;

#define AVERAGE_LENGTH 1

/**
 * TODO: Multi thread this!!!
 */
class KINECT_API Kinect
{

public:

	ICoordinateMapper* mapper;

	Kinect(unsigned int w, unsigned int h);
	~Kinect();

	bool init();
	void update();

	void depth_to_camera(int d_x, int d_y, float& cam_x, float& cam_y, float& cam_z);

	unsigned short int get_depth_data_median(int x, int y);
	unsigned short int get_depth_data_mean(int x, int y);
	unsigned short int get_depth_data_raw(int x, int y);

	unsigned int get_width() const;
	unsigned int get_height() const;

private:

	unsigned int width;
	unsigned int height;
	unsigned int current_frame;
	unsigned int max_frame;

	unsigned short int* data[AVERAGE_LENGTH];

	IKinectSensor* sensor;
	IDepthFrameReader* depth_reader;
	IDepthFrame* frame = NULL;

};

extern int float_compare(const void* a, const void* b);