// Required kinect includes
#include <Windows.h>
#include <Ole2.h>
#include <Kinect.h>

#include "kinect_base.h"

#include <iostream>

Kinect::Kinect(unsigned int w, unsigned int h)
{
	this->width = w;
	this->height = h;
	this->sensor = nullptr;
	this->depth_reader = nullptr;
	this->current_frame = 0;
	this->max_frame = 0;
}

Kinect::~Kinect()
{
	
}

void Kinect::depth_to_camera(int d_x, int d_y, float& cam_x, float& cam_y, float& cam_z)
{
	CameraSpacePoint camera_space;
	DepthSpacePoint depth_space;
	int index = d_x + d_y * width;
	unsigned short int depth_val = data[0][index];
	depth_space.X = d_x;
	depth_space.Y = d_y;
	
	mapper->MapDepthPointToCameraSpace(depth_space, depth_val, &camera_space);

	cam_x = camera_space.X;
	cam_y = camera_space.Y;
	cam_z = camera_space.Z;
}

bool Kinect::init()
{
	for (int i = 0; i < AVERAGE_LENGTH; i++)
		data[i] = new unsigned short int[width * height];

	if (FAILED(GetDefaultKinectSensor(&sensor))) 
	{
		std::cerr << "Failed to initialize the kinect!" << std::endl;
		return false;
	}
	if (sensor) 
	{
		std::cout << "Kinect successfully initialized!" << std::endl;

		mapper = nullptr;

		sensor->Open();

		IDepthFrameSource* depth_source = NULL;
		sensor->get_DepthFrameSource(&depth_source);
		depth_source->OpenReader(&depth_reader);

		if (depth_source)
		{
			depth_source->Release();
			depth_source = NULL;
		}

		sensor->get_CoordinateMapper(&mapper);

		return true;
	}
	else
	{
		std::cerr << "Failed to initialize the kinect!" << std::endl;
		return false;
	}
}

void Kinect::update()
{
	if(depth_reader)
	{
		if (SUCCEEDED(depth_reader->AcquireLatestFrame(&frame)))
		{
			frame->CopyFrameDataToArray(width * height, data[current_frame]);

			/*		current_frame++;
					if (current_frame >= AVERAGE_LENGTH)
						current_frame = 0;
					if (max_frame < AVERAGE_LENGTH)
						max_frame++;*/
		}

		if (frame) frame->Release();
	}
}

unsigned short int Kinect::get_depth_data_median(int x, int y)
{
	int size = 3;
	float candidates[9];

	int mx = size / 2;
	int values_inserted = 0;

	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			int ax = x + (i - mx);
			int ay = y + (j - mx);
			int index = ax + ay * width;

			if (ax >= 0 && ay >= 0 && ax < width && ay < height)
			{
				float value = data[0][index];
				int sort_index = values_inserted;
				while (sort_index > 0 && candidates[sort_index - 1] > value)
				{
					candidates[sort_index] = candidates[sort_index - 1];
					sort_index--;
				}
				candidates[sort_index] = value;

				values_inserted++;
			}

		}
	}

	return (unsigned short int) (candidates[values_inserted / 2]);
}

unsigned short int Kinect::get_depth_data_mean(int x, int y)
{
	int sample_size = 3;
	int middle_sample = sample_size / 2;
	int sample_start = -middle_sample;
	int sample_end = middle_sample;

	int samples = 0;
	float value_sum = 0.0f;

	for (int i = sample_start; i < sample_end; i++)
	{
		for (int j = sample_start; j < sample_end; j++)
		{
			int ax = x + i;
			int ay = y + j;

			if (ax >= 0 && ay >= 0 && ax < width && ay < height)
			{
				samples++;
				value_sum += get_depth_data_raw(x + i, y + j);
			}
		}
	}

	float value = value_sum / samples;

	return (unsigned short int) ((float) value);
}

unsigned short int Kinect::get_depth_data_raw(int x, int y)
{
	unsigned short int value = data[0][x + y * width];
	
	// Depth clipping
	if (value < 600)
		value = 600;
	if (value > 5000)
		value = 5000;
		
	return value;
}

unsigned int Kinect::get_width() const
{
	return width;
}

unsigned int Kinect::get_height() const
{
	return height;
}

int float_compare(const void* a, const void* b)
{
	return *static_cast<const int*>(a) - *static_cast<const int*>(b);
}